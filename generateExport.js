const path = require('path').posix;
const fs = require('fs/promises');

const custonComponents = [];
const customDirectories = [];
const customModules = [
    'export * as Icons from \'./icons\';',
];

(async function() {
    const dir = path.join(__dirname, 'src/components');
    const srcDir = path.join(__dirname, 'src');

    const dirContent = await fs.readdir(dir);

    const components = [];
    const modules = [];
    const directories = [];
    for(const fileName of dirContent) {
        const filePath = path.join(dir, fileName);
        const relPath = path.relative(srcDir, filePath);

        if((await fs.lstat(filePath)).isDirectory()) {
            directories.push(`export * from './${relPath}';`);
        } else {
            const ext = path.extname(filePath);
            const baseName = path.basename(filePath, ext);
            if (ext === '.vue') {
                components.push(`export {default as ${baseName}} from './${relPath}';`);
            } else if (ext === '.ts') {
                const baseRelPath = path.relative(srcDir, path.join(dir, path.basename(filePath, ext)));
                modules.push(`export * from './${baseRelPath}';`);
            }
        }
    }

    components.push(...custonComponents);
    directories.push(...customDirectories);
    modules.push(...customModules);

    const result = joinRecursive([components, directories, modules], '\r\n');
    console.log(result);
})();

function joinRecursive(data, sep) {
    return data.map((element) => {
        if (Array.isArray(element[0])) {
            return joinRecursive(element, sep);
        } else if (Array.isArray(element)){
            return element.join(sep);
        } else {
            return element;
        }
    }).join(sep);
}