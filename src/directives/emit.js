import {toHandlerKey, camelize} from 'vue';

export default function (vnode, name, data) {
    name = toHandlerKey(camelize(name));
    const handlers = vnode.props;
    if (handlers && handlers[name]) {
        const handler = handlers[name];
        if (typeof handler === 'function') {
            return handler(data);
        }
    }
}
