/**
 * Добавляет поведение драга с пиксельной задержкой
 * параметры:
 * @param {Number} [pixelTreshold=5] - пиксельная задержка
 * @param {Number} [clickDistance=0] - расстояние, до которого сработает клик
 * @param {Function} [filter=()=>true] - функция фильтра элеметов, на которых должен сработать драг
 * @param {String} [overlayCursor] - курсор во время драга элемента
 * @param {Function} [subject] - возвращает данные перетаскиваемого объекта. Выполняется один раз при начале драга
 *                              {@see https://github.com/d3/d3-drag#drag_subject}
 */

import d3 from '../modules/d3modules';
import emit from './emit';

const DEF_PIXEL_TRESHOLD = 5;
const DEF_CLICK_DISTANCE = 5;
const DEF_FILTER = () => true;
const DEF_CONTAINER = () => document.body;
const rgTouchEvent = /touch/;

let pixelTreshold = DEF_PIXEL_TRESHOLD;
let clickDistance = DEF_CLICK_DISTANCE;
let filter = DEF_FILTER;
let container = DEF_CONTAINER;
let overlayCursor = null;
let subject = null;

function unbind(el) {
    if (el.vDraggableDrag) {
        el.vDraggableDrag
            .on('start', null)
            .on('drag', null)
            .on('end', null);
        el.vDraggableDrag = null;
    }
    el.vDraggableCancel = null;
    d3.select(el).on('touchstart.draggable', null);
}

let overlay = null;

/**
 * Создает и возвращает DOM-элемент оверлея
 * @return {Node}
 */
function createOverlay(cursor) {
    const overlay = document.createElement('div');
    overlay.classList.add('fixed');
    overlay.classList.add('fit');
    overlay.style.zIndex = '5999';
    overlay.style.top = '0';
    overlay.style.left = '0';
    overlay.style.cursor = cursor;
    return overlay;
}


export default {
    name: 'draggable',
    beforeMount: function (el, binding, vnode) {
        updateProps(binding);
        let distance = 0;
        let dragAvailable = false;
        let __overlayCursor = overlayCursor;
        let dragging = false;
        d3.select(el)
            .on('touchstart.draggable', (ev) => {
                if (isMultitouch(ev)) {
                    cancelDrag();
                }
            })
            .call(
                el.vDraggableDrag = d3.drag()
                    .container(container)
                    .clickDistance(clickDistance)
                    .filter(filter)
                    .on('start', (ev) => {
                        distance = 0;
                        dragAvailable = false;
                        el.vDraggableDrag.startEvent = ev.sourceEvent;
                        const event = new DragEvent('dragstart', ev);
                        const res = emit(vnode, 'dragstart', event);
                        if (!res && res !== undefined) {
                            unbind(el);
                        }
                    })
                    .on('drag', (ev) => {
                        if (ev.sourceEvent.touches && ev.sourceEvent.touches.length > 1) {
                            cancelDrag();
                            return;
                        }
                        if (__overlayCursor) {
                            if (!dragging) {
                                overlay = createOverlay(__overlayCursor);
                                document.body.appendChild(overlay);
                            }
                            dragging = true;
                        }
                        if (dragAvailable) {
                            emit(vnode, 'drag', ev);
                        } else {
                            distance += Math.sqrt(ev.dx * ev.dx + ev.dy * ev.dy);
                            if (distance > pixelTreshold) {
                                dragAvailable = true;
                                emit(vnode, 'firstdrag', ev);
                                emit(vnode, 'drag', ev);
                            }
                        }
                    })
                    .on('end', (ev) => {
                        if (__overlayCursor) {
                            if (overlay) {
                                dragging = false;
                                overlay.remove();
                            }
                        }
                        if (ev.sourceEvent.vDraggableCancel) {
                            ev.sourceEvent.stopPropagation();
                            emit(vnode, 'dragend', null);
                        } else {
                            emit(vnode, 'dragend', new DragEvent('dragend', ev));
                        }
                    }),
            );
        if (subject) {
            el.vDraggableDrag.subject(subject);
        }
        el.vDraggableCancel = cancelDrag;

        function cancelDrag() {
            if (el.vDraggableDrag && el.dispatchEvent) {
                let ev;
                if (isTouchEvent(el.vDraggableDrag.startEvent)) {
                    ev = new TouchEvent('touchend', el.vDraggableDrag.startEvent);
                } else {
                    ev = new MouseEvent('mouseup', el.vDraggableDrag.startEvent);
                }
                ev.vDraggableCancel = true;
                ev.preventDefault();
                el.dispatchEvent(ev);
            }
        }
    },
    beforeUnmount(el) {
        unbind(el);
    },
    update: function (el, binding) {
        updateProps(binding);
    },
};


function isTouchEvent(event) {
    return rgTouchEvent.test(event.type);
}

function updateProps(binding) {
    pixelTreshold = initOption(binding, 'pixelTreshold', DEF_PIXEL_TRESHOLD);
    clickDistance = initOption(binding, 'clickDistance', DEF_CLICK_DISTANCE);
    container = initOption(binding, 'container', DEF_CONTAINER);
    let customFilter = initOption(binding, 'filter', DEF_FILTER);
    filter = (ev) => !isMultitouch(ev) && customFilter(ev);
    overlayCursor = initOption(binding, 'overlayCursor', null);
    subject = initOption(binding, 'subject', void 0);
}

function isMultitouch(event) {
    return event && event.touches && event.touches.length > 1;
}

function initOption(binding, optName, defValue) {
    return binding.value && (optName in binding.value)
        ? binding.value[optName]
        : defValue;
}

// DragEvent from d3-drag/src/event
function DragEvent(type, {
    sourceEvent,
    subject,
    target,
    identifier,
    active,
    x, y, dx, dy,
    dispatch,
}) {
    Object.defineProperties(this, {
        type: {value: type, enumerable: true, configurable: true},
        sourceEvent: {value: sourceEvent, enumerable: true, configurable: true},
        subject: {value: subject, enumerable: true, configurable: true},
        target: {value: target, enumerable: true, configurable: true},
        identifier: {value: identifier, enumerable: true, configurable: true},
        active: {value: active, enumerable: true, configurable: true},
        x: {value: x, enumerable: true, configurable: true},
        y: {value: y, enumerable: true, configurable: true},
        dx: {value: dx, enumerable: true, configurable: true},
        dy: {value: dy, enumerable: true, configurable: true},
        _: {value: dispatch},
    });
}

DragEvent.prototype.on = function () {
    var value = this._.on.apply(this._, arguments);
    return value === this._ ? this : value;
};