import AppLogo from './components/AppLogo.vue';
import AspectRatio from "./components/AspectRatio.vue";
import ButtonToolbar from './components/ButtonToolbar.vue';
import ControlDraggableInput from './components/ControlDraggableInput.vue';
import FileUploader from './components/FileUploader.vue';
import IconButton from './components/IconButton.vue';
import IconTextButton from './components/IconTextButton.vue';
import MasterLayout from './components/MasterLayout.vue';
import Progress from './components/Progress.vue';
import PropertyLabel from "./components/PropertyLabel.vue";
import SuffixAttribute from './components/SuffixAttribute.vue';
import CropArea from './components/CropArea.vue';
import CropSizeToolbar from './components/CropSizeToolbar.vue';
import CropAspectRatioToolbar from './components/CropAspectRatioToolbar.vue';
import VideoCustomizerLayout from './components/VideoCustomizerLayout.vue';
import VideoMarker from './components/VideoMarker.vue';
import VideoPlaybackTimeline from './components/VideoPlaybackTimeline.vue';
import MuteButton from './components/MuteButton.vue';
import PlayButton from './components/PlayButton.vue';
import FooterToolbar from './components/FooterToolbar.vue';
import ExportButton from './components/ExportButton.vue';
import MasterCheckbox from './components/MasterCheckbox.vue';
import RemoveSoundControl from './components/RemoveSoundControl.vue';
import MasterDraggableInput from './components/MasterDraggableInput.vue';
import ToggleIconButton from './components/ToggleIconButton.vue';
import DropdownButton from './components/DropdownButton.vue';
import type {IEventLoadedMasterVideoPlayer, IMenuItemData,} from './components';
import FileSelection from './components/FileSelection.vue';
import {convertSeconds} from "./utils/time";
import {createEvent, listenDom} from './utils/event';
import {isMobile} from './utils/platform';
import ResultOutputPanel from './components/ResultOutputPanel.vue';
import ToggleIconTextButton from './components/ToggleIconTextButton.vue';
import MasterFooterToolbar from './components/MasterFooterToolbar.vue';
import MasterProgress from './components/MasterProgress.vue';
import MasterResultStage from './components/MasterResultStage.vue';
import PropertyColor from './components/PropertyColor.vue';
import VideoPreloader from './components/VideoPreloader.vue';
import ConfirmationDialog from './components/ConfirmationDialog.vue';
import AppVersion from './components/AppVersion.vue';
import PlaybackTimeline from './components/PlaybackTimeline.vue';
import MTimeDraggableInput from './components/MTimeDraggableInput.vue';
import ResultTextHeader from './components/ResultTextHeader.vue';
import ResultTextSize from './components/ResultTextSize.vue';
import ResultTextDuration from './components/ResultTextDuration.vue';
import ResultTextResolution from './components/ResultTextResolution.vue';
import ResultDescriptionArea from './components/ResultDescriptionArea.vue';
import InformationDialog from './components/InformationDialog.vue';
import MasterSelect from './components/MasterSelect.vue';
import TextAlignToggle from './components/TextAlignToggle.vue';
import SimpleColorSelection from './components/SimpleColorSelection.vue';
import FontSizeListSelection from './components/FontSizeListSelection.vue';
import draggable from './directives/draggable';
import {useDragMove} from "./components/useDragMove";
import {useDragRotate} from "./components/useDragRotate";
import MasterInput from "./components/MasterInput.vue";

export {
    AppLogo,
    AppVersion,
    AspectRatio,
    ButtonToolbar,
    ConfirmationDialog,
    ControlDraggableInput,
    CropArea,
    CropAspectRatioToolbar,
    CropSizeToolbar,
    DropdownButton,
    ExportButton,
    FileSelection,
    FileUploader,
    FontSizeListSelection,
    FooterToolbar,
    IEventLoadedMasterVideoPlayer,
    IMenuItemData,
    IconButton,
    IconTextButton,
    InformationDialog,
    MTimeDraggableInput,
    MasterCheckbox,
    MasterDraggableInput,
    MasterFooterToolbar,
    MasterInput,
    MasterLayout,
    MasterProgress,
    MasterResultStage,
    MasterSelect,
    MuteButton,
    PlayButton,
    PlaybackTimeline,
    Progress,
    PropertyColor,
    PropertyLabel,
    RemoveSoundControl,
    ResultDescriptionArea,
    ResultOutputPanel,
    ResultTextDuration,
    ResultTextHeader,
    ResultTextResolution,
    ResultTextSize,
    SimpleColorSelection,
    SuffixAttribute,
    TextAlignToggle,
    ToggleIconButton,
    ToggleIconTextButton,
    VideoCustomizerLayout,
    VideoMarker,
    VideoPlaybackTimeline,
    VideoPreloader,
    convertSeconds,
    createEvent,
    draggable,
    isMobile,
    listenDom,
    useDragMove,
    useDragRotate,
};

export * as Icons from './icons';
export * from './components/trim';
export * from './components/add-text';
export * from './components/useDragResize';
export {default as MasterSelectAudio} from './components/MasterSelectAudio.vue';
export {default as MasterAudioPlayerHidden} from './components/MasterAudioPlayerHidden.vue';
export {default as ControlAudio} from './components/ControlAudio.vue';
export {default as ConfirmationRemoveAudioDialog} from './components/ConfirmationRemoveAudioDialog.vue';
export {default as CustomizerItem} from './components/CustomizerItem.vue';
export * from './components/useAudioPlayer';
export {default as ResultOutputPanelItem} from './components/ResultOutputPanelItem.vue';
export {default as AccountProfile} from './components/AccountProfile.vue';
export {default as LinkAsButton} from './components/LinkAsButton.vue';
export {default as LinkButton} from './components/LinkButton.vue';
export {default as MasterAuth} from './components/MasterAuth.vue';
export {default as MasterHeader} from './components/MasterHeader.vue';
export {default as Modal} from './components/Modal.vue';
export {default as OAuthProviderButton} from './components/OAuthProviderButton.vue';
export {default as OAuthProviders} from './components/OAuthProviders.vue';
export {default as RecoverPassword} from './components/RecoverPassword.vue';
export {default as RecoverPasswordModal} from './components/RecoverPasswordModal.vue';
export {default as SignIn} from './components/SignIn.vue';
export {default as SignInModal} from './components/SignInModal.vue';
export {default as SignUp} from './components/SignUp.vue';
export {default as SignUpModal} from './components/SignUpModal.vue';
export {default as CanvasVideoPlayer} from './components/CanvasVideoPlayer.vue';
export {default as MasterButtonToggleGroup} from './components/MasterButtonToggleGroup.vue';
export {default as MasterSkeleton} from './components/MasterSkeleton.vue';
export {default as MasterTariffSelect} from './components/MasterTariffSelect.vue';
export {default as GridBorderHControl} from './components/add-text/Grid/GridBorderHControl.vue';
export {default as GridBorderVControl} from './components/add-text/Grid/GridBorderVControl.vue';
export {default as GridCloseControl} from './components/add-text/Grid/GridCloseControl.vue';
export {default as MasterGifPlayer} from './components/MasterGifPlayer.vue';
export {default as MasterLogo} from './components/MasterLogo.vue';
export {default as HeaderAuthorization} from './components/HeaderAuthorization.vue';
export {default as StageHeader} from './components/StageHeader.vue';
export {default as AccountHeaderUserProfileControl} from './components/AccountHeaderUserProfileControl.vue';
export {default as ProjectsListControl} from './components/ProjectsListControl.vue';
export {default as ScenePlayerToolbar} from './components/ScenePlayerToolbar.vue';
export {default as ScenePlayerCurrentTime} from './components/ScenePlayerCurrentTime.vue';
export {default as MasterBackgroundPlayer} from './components/players/MasterBackgroundPlayer.vue';
export {default as MasterVideoPlayer} from './components/players/MasterVideoPlayer.vue';
export {default as MasterImagePlayer} from './components/players/MasterImagePlayer.vue';
export {default as MInputSlider} from './components/MInputSlider.vue';
export {default as ControlZoom} from './components/ControlZoom.vue';
export {default as FocusShakeAnimation} from './components/FocusShakeAnimation.vue';
export {default as ColorPickerButton} from './components/ColorPickerButton.vue';
export {default as AxisSceneTranslateControl} from './components/AxisSceneTranslateControl.vue';
export {default as SimpleToolbar} from './components/SimpleToolbar.vue';
export {default as MasterTitle} from './components/MasterTitle.vue';
export {default as EffectMasterFooterToolbar} from './components/EffectMasterFooterToolbar.vue';
export {default as CustomizerHeader} from './components/CustomizerHeader.vue';
export {default as AccountProfilePanel} from './components/AccountProfilePanel.vue';

export * from './modules/showError';
export * from './modules/errorNotify';
export * from './modules/infoModal';
export * from './modules/PageLifecycle';
export * from './modules/toolsList';
export * from './modules/saveToService';
export * from './modules/loadFileIdFromUrl';
export * from './modules/requestWithAuthorize';
export * from './modules/quasar';
export * from './modules/authorization';
export * from './components/useMobileAccept';