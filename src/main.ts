import {createApp} from 'vue'
import App from './App.vue'
import {Quasar} from 'quasar'
// Подключение всех стилей quasar
import 'quasar/src/css/index.sass';
// Подключение дополнительных стилей quasar
import 'quasar/src/css/flex-addon.sass';
// Подключение дополнительных классов цвета
import './styles/custom-color-class.scss';
import i18n from '@mixapixa-masters/locales';

createApp(App)
    .use(Quasar, {
        plugins: {}
    })
    .use(i18n)
    .mount('#app')