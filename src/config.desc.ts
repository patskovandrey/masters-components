export interface MPAppConfig {
    devPort?: number,
    title: string,
    serviceUrl: string,
    socketUrl: string
}