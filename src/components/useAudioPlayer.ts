import type {Ref, ComputedRef} from "vue";
import {ref} from "vue";
import type MasterAudioPlayerHidden from "./MasterAudioPlayerHidden.vue";

export function useAudioPlayer(
    audioLoop: Ref<boolean> | ComputedRef<boolean>,
    audioDuration: Ref<number> | ComputedRef<number>,
    isPlaying: Ref<boolean> | ComputedRef<boolean>,
) {
    const refAudio = ref<typeof MasterAudioPlayerHidden | null>(null);

    function playAudio(time: number) {
        if (!refAudio.value) return;
        setAudioTime(time);
        if (time < audioDuration.value || audioLoop.value) {
            refAudio.value.play();
        }
    }

    function pauseAudio() {
        if (!refAudio.value) return;
        refAudio.value.pause();
    }

    function setAudioTime(time: number) {
        if (!refAudio.value) return;
        const duration = audioDuration.value;
        if (audioLoop.value && duration) {
            refAudio.value.currentTime = time % duration;
        } else {
            refAudio.value.currentTime = Math.min(time, duration);
        }
    }

    /**
     * Обработчик события ended плеера
     */
    function onAudioPlayEnded() {
        if (audioLoop.value && refAudio.value && isPlaying.value) {
            refAudio.value.currentTime = 0;
            refAudio.value.play();
        }
    }

    return {
        refAudio,
        playAudio,
        pauseAudio,
        setAudioTime,
        onAudioPlayEnded,
    }
}