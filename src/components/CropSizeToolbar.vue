<script lang="ts" setup>
  /**
   * Панель инструментов: изменение размера области кадрирования.
   *
   * Содержит поля ввода ширины и высоты области кадрирования, кнопку для включения режима
   * сохранения соотношения сторон.
   *
   * Свойства: см. `CropSizeToolbarProps`.
   * События: см. `CropSizeToolbarEmits`.
   */

  import { computed } from 'vue';
  import type { CropAspectRatio, CropConstraints, Size } from '@mixapixa-masters/types';
  import type { IOptionsRange } from './index';
  import { Icons, MasterDraggableInput, PropertyLabel, ToggleIconButton } from '../index';
  import { setupI18n } from '@mixapixa-masters/locales';

  interface CropSizeToolbarProps {
    // область кадрирования
    crop: Size;
    // ограничения области кадрирования
    cropConstraints: CropConstraints;
    // желаемое соотношение сторон области кадрирования
    cropAspectRatio: CropAspectRatio;
    // сохранять соотношение сторон области кадрирования при изменении её размеров
    cropPreserveAspectRatio: boolean;
    preserveDisable?: boolean;
    preloading?: boolean;
  }

  interface CropSizeToolbarEmits {
    // изменились размеры области кадрирования
    (event: 'size:changed', payload: Size): void,

    // изменился признак сохранения соотношения сторон области кадрирования
    (event: 'size:preserve-aspect-ratio-changed', payload: boolean): void,

    (event: 'update:cropPreserveAspectRatio', payload: boolean): void,
  }

  const props = defineProps<CropSizeToolbarProps>();
  const emit = defineEmits<CropSizeToolbarEmits>();
  const { t } = setupI18n();
  /**
   * Параметры поля ввода ширины области кадрирования.
   * TODO: учитывать соотношение сторон, если нужно
   */
  const cropWidthInputOptions = computed((): IOptionsRange => ({
    min: props.cropConstraints.minWidth,
    max: props.cropConstraints.maxWidth,
    suffix: 'px',
  }));

  /**
   * Параметры поля ввода ширины области кадрирования.
   * TODO: учитывать соотношение сторон, если нужно
   */
  const cropHeightInputOptions = computed((): IOptionsRange => ({
    min: props.cropConstraints.minHeight,
    max: props.cropConstraints.maxHeight,
    suffix: 'px',
  }));

  function formatRound(e: number | null) {
    if (e === null) return '';
    return Math.round(e).toString();
  }

  function parseRound(e: string) {
    return Math.round(+e);
  }

  /**
   * Желаемое отношение сторон области кадрирования в виде вещественного числа.
   * Не определено, если отношение сторон не задано или одна из его составляющих равна нулю.
   */
  const aspectRatio = computed(() => {
    if (!props.cropAspectRatio.width || !props.cropAspectRatio.height) {
      return null;
    } else {
      return props.cropAspectRatio.width / props.cropAspectRatio.height;
    }
  });

  /**
   * Обработчики событий.
   */

  /**
   * Обработчик клика по кнопке сохранения соотношения сторон.
   */
  const onPreserveAspectRatioButtonClick = () => {
    emit(
        'size:preserve-aspect-ratio-changed',
        !props.cropPreserveAspectRatio,
    );
  };

  /**
   * Обработчик события ввода новой ширины области кадрирования.
   * @param value
   */
  const onCropWidthInputChanged = (value: string | number | null) => {
    if (value == null) {
      return;
    }

    const newWidth = typeof value === 'number' ? Math.round(value) : parseInt(value);
    if (!isNaN(newWidth)) {
      if (props.cropPreserveAspectRatio && aspectRatio.value) {
        // сохранить соотношение сторон новых размеров области кадрирования
        const newHeight = Math.round(newWidth / aspectRatio.value);
        emit('size:changed', {
          width: newWidth,
          height: newHeight,
        });
      } else {
        emit('size:changed', {
          width: newWidth,
          height: props.crop.height,
        });
      }
    } else {
      console.warn('CropSizeToolbar: width should be an integer');
    }
  };

  /**
   * Обработчик события ввода новой высоты области кадрирования.
   * @param value
   */
  const onCropHeightInputChanged = (value: string | number | null) => {
    if (value == null) {
      return;
    }

    const newHeight = typeof value === 'number' ? Math.round(value) : parseInt(value);
    if (!isNaN(newHeight)) {
      if (props.cropPreserveAspectRatio && aspectRatio.value) {
        // сохранить соотношение сторон новых размеров области кадрирования
        const newWidth = Math.round(newHeight * aspectRatio.value);
        emit('size:changed', {
          width: newWidth,
          height: newHeight,
        });
      } else {
        emit('size:changed', {
          width: props.crop.width,
          height: newHeight,
        });
      }
    } else {
      console.warn('CropSizeToolbar: height should be an integer');
    }
  };
</script>

<style scoped>
    .container {
        display: grid;
        grid-template-columns: 1fr 60px 24px 60px;
        grid-template-rows: 1fr;
        grid-auto-columns: 1fr;
        gap: 0 8px;
        grid-auto-flow: row dense;
        align-items: center;
        grid-template-areas:
    ". . . .";
    }

    @media screen and (max-width: 1023px) {
        .container {
            grid-template-columns: minmax(40px, auto) minmax(40px, 100px) 24px minmax(40px, 100px);
        }
    }
</style>

<template>
  <div class="container">
    <PropertyLabel
      :preloading="preloading"
      class="col-grow"
      :label="t('sizePx')"
    />
    <MasterDraggableInput
      :preloading="preloading"
      class="text-master-brand col"
      :value="props.crop.width"
      :options="cropWidthInputOptions"
      :format="formatRound"
      @change="onCropWidthInputChanged"
      @input:drag="onCropWidthInputChanged"
    />
    <ToggleIconButton
      class="rotate-90"
      :icon="Icons.link"
      :disable="preserveDisable"
      :model-value="cropPreserveAspectRatio"
      @update:modelValue="onPreserveAspectRatioButtonClick"
    />
    <MasterDraggableInput
      :preloading="preloading"
      class="text-master-brand col"
      :value="props.crop.height"
      :options="cropHeightInputOptions"
      :parse="parseRound"
      :format="formatRound"
      @change="onCropHeightInputChanged"
      @input:drag="onCropHeightInputChanged"
    />
  </div>
</template>