import type {Position} from "@mixapixa-masters/types";

const R2D = 180 / Math.PI;

export function useDragRotate() {
    let _center: Position;
    let _startAngle = 0;
    let _rotate = 0;

    function getClientCoords(event: any): Position {
        return {
            x: event.sourceEvent.clientX || event.sourceEvent?.touches[0].clientX,
            y: event.sourceEvent.clientY || event.sourceEvent?.touches[0].clientY
        }
    }

    function dragRotateStart(event: any, element: HTMLElement) {
        event.sourceEvent.preventDefault();
        if (!element) return;

        if (event.sourceEvent?.touches?.length > 1) {
            return;
        }

        const bbox = element.getBoundingClientRect();
        const top = bbox.top;
        const left = bbox.left;
        const height = bbox.height;
        const width = bbox.width;

        _center = {
            x: left + (width / 2),
            y: top + (height / 2)
        };

        const clientCoord = getClientCoords(event);
        const x = clientCoord.x - _center.x;
        const y = clientCoord.y - _center.y;

        _startAngle = R2D * Math.atan2(y, x);
    }

    function dragRotate(event: any): number {
        event.sourceEvent.preventDefault();
        const clientCoord = getClientCoords(event);

        const x = clientCoord.x - _center.x;
        const y = clientCoord.y - _center.y;
        const d = R2D * Math.atan2(y, x);

        _rotate = d - _startAngle;

        return _rotate;
    }

    function dragRotateEnd(): number {
        return _rotate;
    }

    return {
        dragRotateStart,
        dragRotate,
        dragRotateEnd,
    }
}