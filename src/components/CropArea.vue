<template>
  <div class="crop-area absolute absolute-top absolute-left fit">
    <div ref="dragContainer" class="crop_area__container absolute absolute-top absolute-left fit overflow-hidden">
      <div ref="dragBox" class="crop-area__box absolute" :style="{
        left: `${cropOriginX}px`,
        top: `${cropOriginY}px`,
        width: `${cropWidth}px`,
        height: `${cropHeight}px`
      }">
        <span class="crop-area__line crop-area__line_n absolute full-width"></span>
        <span class="crop-area__line crop-area__line_e absolute full-height"></span>
        <span class="crop-area__line crop-area__line_s absolute full-width"></span>
        <span class="crop-area__line crop-area__line_w absolute full-height"></span>
        <span class="crop-area__side crop-area__side_n absolute gt-sm" ref="pointN"
              :style="{ left: `${cropCenterX - 8}px` }"></span>
        <span class="crop-area__side crop-area__side_e absolute gt-sm" ref="pointE"
              :style="{ top: `${cropCenterY - 8}px` }"></span>
        <span class="crop-area__side crop-area__side_s absolute gt-sm" ref="pointS"
              :style="{ left: `${cropCenterX - 8}px` }"></span>
        <span class="crop-area__side crop-area__side_w absolute gt-sm" ref="pointW"
              :style="{ top: `${cropCenterY - 8}px` }"></span>
        <span class="crop-area__corner crop-area__corner_ne absolute gt-sm"></span>
        <span class="crop-area__corner crop-area__corner_se absolute "></span>
        <span class="crop-area__corner crop-area__corner_sw absolute gt-sm"></span>
        <span class="crop-area__corner crop-area__corner_nw absolute gt-sm"></span>
      </div>
    </div>
  </div>
</template>

<script lang="ts" setup>

  /**
   * Компонент выбора области кадрирования.
   *
   * Предназначен для выбора некоторой области контента с помощью рамки,
   * положение и размер которой изменяются перетаскиванием. При изменении размеров области кадрирования
   * может учитываться желаемое соотношение её сторон.
   *
   * Компонент должен быть размещен в одном контейнере с элементом, содержащим контент
   * (например, видео) и иметь с ним один экранный размер; контент должен сохранять соотношение сторон.
   * Реальный масштаб контента может отличаться от отображаемого.
   *
   * При перемещении и изменении размеров области кадрирования генерируются соответствующие события,
   * содержащие новую область кадрирования, без валидации (валидация возложена на получателя события).
   *
   * Свойства: см. `CropAreaProps`.
   * События: см. `CropAreaEmits`.
   */

  import { computed, onBeforeUnmount, onMounted, ref } from "vue";
  import type { CropAspectRatio, Crop } from "@mixapixa-masters/types";
  import { drag } from 'd3-drag';
  import { select } from 'd3-selection';


  /**
   * Типы данных.
   */

  /**
   * Начальные данные для жеста перетаскивания.
   */
  interface DragInitialData {
    initialX: number,
    initialY: number,
    initialCropOriginX: number,
    initialCropOriginY: number,
    initialCropWidth: number,
    initialCropHeight: number,
  }

  /**
   * Событие перетаскивания (начало, промежуточное событие или конец).
   */
  interface DragEvent {
    x: number,
    y: number,
    subject: DragInitialData,
  }

  /**
   * Свойства компонента.
   */
  interface CropAreaProps {
    // область кадрирования
    crop: Crop,
    // соотношение сторон области кадрирования
    cropAspectRatio: CropAspectRatio;
    // учитывать ли соотношение сторон области кадрирования при изменении ее размеров
    cropPreserveAspectRatio: boolean;
    // масштаб: отношение экранного размера области контента к реальному,
    // например, отображаемого размера видео к его реальному (intrinsic) размеру;
    // предполагается, что контент сохраняет соотношение сторон
    cropScale?: number,
  }

  /**
   * События компонента.
   */
  interface CropAreaEmits {
    // область кадрирования перемещена
    (event: 'crop:moved', payload: Crop): void

    // изменен размер области кадрирования
    (event: 'crop:resized', payload: Crop): void
  }

  const props = defineProps<CropAreaProps>();
  const emit = defineEmits<CropAreaEmits>();

  const dragContainer = ref();
  const dragBox = ref();


  /**
   * Перевести число из реального в отображаемый масштаб.
   * Если число не указано, возвращает 0.
   * Если масштаб (`props.cropScale`) не указан или равен нулю, возвращает исходное число.
   * @param x число (реальная координата или размер).
   */
  const toDisplayScale = (x: number | null): number => {
    if (x == null) {
      return 0;
    }

    if (props.cropScale) {
      return x * props.cropScale;
    } else {
      return x;
    }
  };

  /**
   * Перевести число из отображаемого в реальный масштаб.
   * Если число не указано, возвращает 0.
   * Если масштаб (`props.cropScale`) не указан или равен нулю, возвращает исходное число.
   * Округляет результат до целого числа.
   * @param x число (экранная координата или размер).
   */
  const toRealScale = (x: number | null): number => {
    if (x == null) {
      return 0;
    }

    if (props.cropScale) {
      return Math.round(x / props.cropScale);
    } else {
      return x;
    }
  };

  /**
   * Размеры и позиции элементов области кадрирования (экранные координаты, в пикселах).
   */

  const cropOriginX = computed(() => {
    return toDisplayScale(props.crop.x);
  });

  const cropOriginY = computed(() => {
    return toDisplayScale(props.crop.y);
  });

  const cropWidth = computed(() => {
    return toDisplayScale(props.crop.width);
  });

  const cropHeight = computed(() => {
    return toDisplayScale(props.crop.height);
  });

  const cropCenterX = computed(() => {
    const centerX = props.crop.width / 2;
    return toDisplayScale(centerX);
  });

  const cropCenterY = computed(() => {
    const centerY = props.crop.height / 2;
    return toDisplayScale(centerY);
  });

  /**
   * Желаемое отношение сторон области кадрирования в виде вещественного числа.
   * По умолчанию 1.
   */
  const aspectRatio = computed(() => {
    if (!props.cropAspectRatio.width || !props.cropAspectRatio.height) {
      return 1;
    } else {
      return props.cropAspectRatio.width / props.cropAspectRatio.height;
    }
  });


  /**
   * Обработчики событий.
   */

  /**
   * Обработчик события перетаскивания области кадрирования.
   * @param event
   */
  const onBoxDrag = (event: DragEvent) => {
    const initialCropOriginX = event.subject.initialCropOriginX;
    const initialCropOriginY = event.subject.initialCropOriginY;
    const dx = event.x - event.subject.initialX;
    const dy = event.y - event.subject.initialY;

    const x = toRealScale(initialCropOriginX + dx);
    const y = toRealScale(initialCropOriginY + dy);
    const width = toRealScale(cropWidth.value);
    const height = toRealScale(cropHeight.value);

    const newCrop = {x, y, width, height};
    emit("crop:moved", newCrop);
  }

  /**
   * Обработчик события перетаскивания верхней (N) стороны области кадрирования.
   * @param event
   */
  const onNSideDrag = (event: DragEvent) => {
    const initialCropOriginX = event.subject.initialCropOriginX;
    const initialCropOriginY = event.subject.initialCropOriginY;
    const initialCropWidth = event.subject.initialCropWidth;
    const initialCropHeight = event.subject.initialCropHeight;
    const dy = event.y - event.subject.initialY;

    const x = toRealScale(initialCropOriginX);
    const y = toRealScale(initialCropOriginY + dy);
    const height = toRealScale(initialCropHeight - dy);
    const width = props.cropPreserveAspectRatio ?
        Math.round(height * aspectRatio.value) : toRealScale(initialCropWidth);
    const newCrop = {x, y, width, height};

    emit("crop:resized", newCrop);
  };

  /**
   * Обработчик события перетаскивания правой (E) стороны области кадрирования.
   * @param event
   */
  const onESideDrag = (event: DragEvent) => {
    const initialCropOriginX = event.subject.initialCropOriginX;
    const initialCropOriginY = event.subject.initialCropOriginY;
    const initialCropWidth = event.subject.initialCropWidth;
    const initialCropHeight = event.subject.initialCropHeight;
    const dx = event.x - event.subject.initialX;

    const x = toRealScale(initialCropOriginX);
    const y = toRealScale(initialCropOriginY);
    const width = toRealScale(initialCropWidth + dx);
    const height = props.cropPreserveAspectRatio ?
        Math.round(width / aspectRatio.value) : toRealScale(initialCropHeight);
    const newCrop = {x, y, width, height};

    emit("crop:resized", newCrop);
  };

  /**
   * Обработчик события перетаскивания нижней (S) стороны области кадрирования.
   * @param event
   */
  const onSSideDrag = (event: DragEvent) => {
    const initialCropOriginX = event.subject.initialCropOriginX;
    const initialCropOriginY = event.subject.initialCropOriginY;
    const initialCropWidth = event.subject.initialCropWidth;
    const initialCropHeight = event.subject.initialCropHeight;
    const dy = event.y - event.subject.initialY;

    const x = toRealScale(initialCropOriginX);
    const y = toRealScale(initialCropOriginY);
    const height = toRealScale(initialCropHeight + dy);
    const width = props.cropPreserveAspectRatio ?
        Math.round(height * aspectRatio.value) : toRealScale(initialCropWidth);
    const newCrop = {x, y, width, height};

    emit("crop:resized", newCrop);
  };

  /**
   * Обработчик события перетаскивания левой (W) стороны области кадрирования.
   * @param event
   */
  const onWSideDrag = (event: DragEvent) => {
    const initialCropOriginX = event.subject.initialCropOriginX;
    const initialCropOriginY = event.subject.initialCropOriginY;
    const initialCropWidth = event.subject.initialCropWidth;
    const initialCropHeight = event.subject.initialCropHeight;
    const dx = event.x - event.subject.initialX;

    const x = toRealScale(initialCropOriginX + dx);
    const y = toRealScale(initialCropOriginY);
    const width = toRealScale(initialCropWidth - dx);
    const height = props.cropPreserveAspectRatio ?
        Math.round(width / aspectRatio.value) : toRealScale(initialCropHeight);
    const newCrop = {x, y, width, height};

    emit("crop:resized", newCrop);
  };


  /**
   * Обработчик события перетаскивания правого верхнего (NE) угла области кадрирования.
   * @param event
   */
  const onNECornerDrag = (event: DragEvent) => {
    const initialCropOriginX = event.subject.initialCropOriginX;
    const initialCropOriginY = event.subject.initialCropOriginY;
    const initialCropWidth = event.subject.initialCropWidth;
    const initialCropHeight = event.subject.initialCropHeight;
    const dx = event.x - event.subject.initialX;
    const dy = event.y - event.subject.initialY;

    const x = toRealScale(initialCropOriginX);
    const y = toRealScale(initialCropOriginY + dy);
    const height = toRealScale(initialCropHeight - dy);
    const width = props.cropPreserveAspectRatio ?
        Math.round(height * aspectRatio.value) : toRealScale(initialCropWidth + dx);
    const newCrop = {x, y, width, height};

    emit("crop:resized", newCrop);
  };

  /**
   * Обработчик события перетаскивания правого нижнего (SE) угла области кадрирования.
   * @param event
   */
  const onSECornerDrag = (event: DragEvent) => {
    const initialCropOriginX = event.subject.initialCropOriginX;
    const initialCropOriginY = event.subject.initialCropOriginY;
    const initialCropWidth = event.subject.initialCropWidth;
    const initialCropHeight = event.subject.initialCropHeight;
    const dx = event.x - event.subject.initialX;
    const dy = event.y - event.subject.initialY;

    const x = toRealScale(initialCropOriginX);
    const y = toRealScale(initialCropOriginY);
    const height = toRealScale(initialCropHeight + dy);
    const width = props.cropPreserveAspectRatio ?
        Math.round(height * aspectRatio.value) : toRealScale(initialCropWidth + dx);
    const newCrop = {x, y, width, height};

    emit("crop:resized", newCrop);
  };

  /**
   * Обработчик события перетаскивания левого нижнего (SW) угла области кадрирования.
   * @param event
   */
  const onSWCornerDrag = (event: DragEvent) => {
    const initialCropOriginX = event.subject.initialCropOriginX;
    const initialCropOriginY = event.subject.initialCropOriginY;
    const initialCropWidth = event.subject.initialCropWidth;
    const initialCropHeight = event.subject.initialCropHeight;
    const dx = event.x - event.subject.initialX;
    const dy = event.y - event.subject.initialY;

    const height = toRealScale(initialCropHeight + dy);
    const width = props.cropPreserveAspectRatio ?
        Math.round(height * aspectRatio.value) : toRealScale(initialCropWidth - dx);
    const x = props.cropPreserveAspectRatio ?
        toRealScale(initialCropOriginX + initialCropWidth) - width : toRealScale(initialCropOriginX + dx);
    const y = toRealScale(initialCropOriginY);
    const newCrop = {x, y, width, height};

    emit("crop:resized", newCrop);
  };

  /**
   * Обработчик события перетаскивания левого верхнего (NW) угла области кадрирования.
   * @param event
   */
  const onNWCornerDrag = (event: DragEvent) => {
    const initialCropOriginX = event.subject.initialCropOriginX;
    const initialCropOriginY = event.subject.initialCropOriginY;
    const initialCropWidth = event.subject.initialCropWidth;
    const initialCropHeight = event.subject.initialCropHeight;
    const dx = event.x - event.subject.initialX;
    const dy = event.y - event.subject.initialY;

    const height = toRealScale(initialCropHeight - dy);
    const width = props.cropPreserveAspectRatio ?
        Math.round(height * aspectRatio.value) : toRealScale(initialCropWidth - dx);
    const y = toRealScale(initialCropOriginY + dy);
    const x = props.cropPreserveAspectRatio ?
        toRealScale(initialCropOriginX + initialCropWidth) - width : toRealScale(initialCropOriginX + dx);
    const newCrop = {x, y, width, height};

    emit("crop:resized", newCrop);
  };

  /**
   * Данные, которые будут получены в момент начала перетаскивания
   * и будут доступны в каждом последующем событии в пределах одного жеста перетаскивания
   * (от начального до соответствующего конечного события).
   * Включает координаты начала, ширину и высоту области кадрирования, координаты курсора.
   * @param dragStartEvent событие, начинающее перетаскивание.
   */
  const initialDragData = (dragStartEvent: DragEvent): DragInitialData => {
    return {
      initialX: dragStartEvent.x,
      initialY: dragStartEvent.y,
      initialCropOriginX: cropOriginX.value,
      initialCropOriginY: cropOriginY.value,
      initialCropWidth: cropWidth.value,
      initialCropHeight: cropHeight.value,
    };
  };

  onMounted(() => {
    const dragBoxSelection = select(dragBox.value);
    dragBoxSelection.call(drag()
        .container(dragContainer.value)
        .subject(initialDragData)
        .on("drag", onBoxDrag));


    const dragBoxNSideSelection = dragBoxSelection.selectAll(".crop-area__side_n");
    dragBoxNSideSelection.call(drag<any, any>()
        .container(dragContainer.value)
        .subject(initialDragData)
        .on("start", onNSideDrag)
        .on("drag", onNSideDrag)
        .on("end", onNSideDrag));

    const dragBoxESideSelection = dragBoxSelection.selectAll(".crop-area__side_e");
    dragBoxESideSelection.call(drag<any, any>()
        .container(dragContainer.value)
        .subject(initialDragData)
        .on("start", onESideDrag)
        .on("drag", onESideDrag)
        .on("end", onESideDrag));

    const dragBoxSSideSelection = dragBoxSelection.selectAll(".crop-area__side_s");
    dragBoxSSideSelection.call(drag<any, any>()
        .container(dragContainer.value)
        .subject(initialDragData)
        .on("start", onSSideDrag)
        .on("drag", onSSideDrag)
        .on("end", onSSideDrag));

    const dragBoxWSideSelection = dragBoxSelection.selectAll(".crop-area__side_w");
    dragBoxWSideSelection.call(drag<any, any>()
        .container(dragContainer.value)
        .subject(initialDragData)
        .on("start", onWSideDrag)
        .on("drag", onWSideDrag)
        .on("end", onWSideDrag));


    const dragBoxNECornerSelection = dragBoxSelection.select(".crop-area__corner_ne");
    dragBoxNECornerSelection.call(drag<any, any>()
        .container(dragContainer.value)
        .subject(initialDragData)
        .on("start", onNECornerDrag)
        .on("drag", onNECornerDrag)
        .on("end", onNECornerDrag));

    const dragBoxSECornerSelection = dragBoxSelection.select(".crop-area__corner_se");
    dragBoxSECornerSelection.call(drag<any, any>()
        .container(dragContainer.value)
        .subject(initialDragData)
        .on("start", onSECornerDrag)
        .on("drag", onSECornerDrag)
        .on("end", onSECornerDrag));

    const dragBoxSWCornerSelection = dragBoxSelection.select(".crop-area__corner_sw");
    dragBoxSWCornerSelection.call(drag<any, any>()
        .container(dragContainer.value)
        .subject(initialDragData)
        .on("start", onSWCornerDrag)
        .on("drag", onSWCornerDrag)
        .on("end", onSWCornerDrag));

    const dragBoxNWCornerSelection = dragBoxSelection.select(".crop-area__corner_nw");
    dragBoxNWCornerSelection.call(drag<any, any>()
        .container(dragContainer.value)
        .subject(initialDragData)
        .on("start", onNWCornerDrag)
        .on("drag", onNWCornerDrag)
        .on("end", onNWCornerDrag));
  });

  onBeforeUnmount(() => {
    const dragBoxSelection = select(dragBox.value);
    const dragBoxSidesSelection = dragBoxSelection.selectAll(".crop-area__side");
    const dragBoxCornersSelection = dragBoxSelection.selectAll(".crop-area__corner");

    [dragBoxSelection, dragBoxSidesSelection, dragBoxCornersSelection].forEach((selection) => {
      selection.on(".drag", null);
    });
  });
</script>

<style scoped>
  .crop-area__box {
    box-shadow: 0 0 0 999px rgba(255, 255, 255, .45);
    z-index: 0;
  }

  .crop-area__line {
    background-color: #39b7f4;
    z-index: 1;
  }

  .crop-area__line_n {
    height: 2px;
    top: 0;
  }

  .crop-area__line_e {
    width: 2px;
    left: 0;
  }

  .crop-area__line_s {
    height: 2px;
    bottom: 0;
  }

  .crop-area__line_w {
    width: 2px;
    right: 0;
    z-index: 0;
  }

  .crop-area__side,
  .crop-area__corner {
    display: block;
    background-color: white;
    z-index: 2;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.5), 0 1px 1px rgba(0, 0, 0, 0.5), 0 2px 1px -1px rgba(0, 0, 0, 0.5);
  }

  .crop-area__side:hover,
  .crop-area__corner:hover {
    transform: scale(1.2);
  }

  .crop-area__side_n {
    top: -1px;
    width: 16px;
    height: 4px;
    cursor: n-resize;
  }

  .crop-area__side_e {
    right: -1px;
    width: 4px;
    height: 16px;
    cursor: w-resize;
  }

  .crop-area__side_s {
    bottom: -1px;
    width: 16px;
    height: 4px;
    cursor: s-resize;
  }

  .crop-area__side_w {
    left: -1px;
    width: 4px;
    height: 16px;
    cursor: w-resize;
  }

  .crop-area__corner {
    width: 12px;
    height: 12px;
    border-radius: 50%;
  }

  .crop-area__corner_ne {
    right: -5px;
    top: -5px;
    cursor: ne-resize;
  }

  .crop-area__corner_se {
    right: -5px;
    bottom: -5px;
    cursor: se-resize;
  }

  .crop-area__corner_sw {
    left: -5px;
    bottom: -5px;
    cursor: sw-resize;
  }

  .crop-area__corner_nw {
    top: -5px;
    left: -5px;
    cursor: nw-resize;
  }
</style>