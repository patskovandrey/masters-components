import {resizeFromCenter, resizeFromTopLeft} from "@mixapixa-masters/utils";
import type {Position, Size} from "@mixapixa-masters/types";
import {projection, resizeFromBottomRight} from "@mixapixa-masters/utils/src/transform";

export function useDragResize(resizeType: 'center' | 'topleft' | 'bottomright') {
    let
        // video x
        x0 = 0,
        // video y
        y0 = 0,
        // video w
        w0 = 0,
        // video h
        h0 = 0,
        // rotation angle rad
        a = 0,
        // display x
        _x = 0,
        // display y
        _y = 0;

    const resizeFunction = {
        center: resizeFromCenter,
        topleft: resizeFromTopLeft,
        bottomright: resizeFromBottomRight,
    }[resizeType];

    function dragResizeStart(event: any, data: Position & Size & { rotate: number }) {
        ({
            x: x0, y: y0,
            width: w0, height: h0,
            rotate: a,
        } = data);
        _x = event.x;
        _y = event.y;
    }

    function dragResize(event: any, {
        scale,
        vectorDirection,
        proportional
    }: { scale: number, vectorDirection?: [number, number], proportional?: boolean }): Position & Size | null {
        let dx = event.x - _x;
        let dy = event.y - _y;

        if (vectorDirection) {
            const vecProjection = projection(vectorDirection[0], vectorDirection[1], dx, dy, a);

            dx = vecProjection[0];
            dy = vecProjection[1];

            // Не понятно почему именно так работает, разобраться
            if (vectorDirection[0] < 0) {
                dx *= -1;
                dy *= -1;
            }
            if (vectorDirection[1] < 0) {
                dx *= -1;
                dy *= -1;
            }
        }

        return resizeFunction(
            x0, y0,
            w0, h0,
            a,
            dx, dy,
            scale,
            proportional,
        );
    }

    function dragResizeEnd(event: any, {scale}: { scale: number }): Position & Size | null {
        return dragResize(event, {scale});
    }

    return {
        dragResizeStart,
        dragResize,
        dragResizeEnd,
    };
}