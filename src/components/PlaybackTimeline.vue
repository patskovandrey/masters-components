<script lang="ts" setup>

  /**
   * Компонент: индикатор воспроизведения видео.
   *
   * Позволяет отслеживать и изменять позицию воспроизведения видео.
   *
   * Отображает маркер позиции воспроизведения:
   * - на десктопе - желаемой;
   * - на мобильных устройствах - текущей.
   *
   * Свойства: см. `VideoPlaybackTimelineProps`.
   * События: см. `VideoPlaybackTimelineEmits`.
   */

  import { computed, onBeforeMount, onBeforeUnmount, ref, watch } from 'vue';
  import { Platform, QSlider } from 'quasar';
  import VideoMarker from './VideoMarker.vue';

  type VideoPlaybackPosition = number;
  type VideoDuration = number;

  interface VideoPlaybackTimelineProps {
    duration: VideoDuration;
    time: number;
    disabled?: boolean | undefined;
    trackColor?: string;
    markerColor?: string;
    markerTextColor?: string;
    thumbColor?: string;
    noMarker?: boolean | undefined;
    trackSize?: string | undefined;
    thumbSize?: string | undefined;
  }

  interface VideoPlaybackTimelineEmits {
    (event: 'position:changed', payload: VideoPlaybackPosition): void,
  }

  const props = withDefaults(defineProps<VideoPlaybackTimelineProps>(), {
    trackColor: 'master-brand',
    markerColor: 'master-brand',
    markerTextColor: 'white',
    thumbColor: 'master-brand',
    trackSize: '2px',
    thumbSize: '18px',
  });
  const emit = defineEmits<VideoPlaybackTimelineEmits>();


  /**
   * Форматировать позицию воспроизведения в формат hh:mm:ss.ms
   * (часы:минуты:секунды.миллисекунды).
   * Если позиция составляет меньше одного часа, секция часов не включается в результат.
   * @param position позиция воспроизведения (в секундах).
   */
  const formatPosition = (position: number): string => {
    const hours = Math.floor(position / 3600);
    const minutes = Math.floor(position % 3600 / 60);
    const seconds = Math.floor(position % 3600 % 60);
    const milliseconds = Math.floor((position % 1) * 10);

    const hoursString = hours > 9 ? `${hours}` : `0${hours}`;
    const minutesString = minutes > 9 ? `${minutes}` : `0${minutes}`;
    const secondsString = seconds > 9 ? `${seconds}` : `0${seconds}`;
    const millisecondsString = milliseconds.toString();

    return `${hours ? hoursString + ':' : ''}${minutesString}:${secondsString}.${millisecondsString}`;
  };


  /**
   * Состояние компонента.
   */

  /**
   * Элемент индикатора воспроизведения видео.
   */
  const timelineElement = ref();

  /**
   * Позиция проигрывания видео (в секундах).
   */
  const position = ref<VideoPlaybackPosition>(0);

  /**
   * Состояние компонента (активен/неактивен).
   * В неактивном состоянии отображается только полоска позиции проигрывания,
   * в активном также отображается ручка текущей позиции для перетаскивания и
   * поясняющая надпись над ней, а полоска позиции становится шире.
   */
  const active = ref<boolean>(false);

  /**
   * Состояние перетаскивания текущей позиции.
   */
  const dragging = ref<boolean>(false);

  /**
   * Позиция индикатора воспроизведения видео (по горизонтали).
   */
  const timelineX = ref<number>(0);

  /**
   * Ширина индикатора воспроизведения видео.
   */
  const timelineWidth = ref<number>(0);

  /**
   * Позиция маркера по горизонтали, в пикселах.
   */
  const markerX = ref<number>(0);

  const slider = ref<QSlider | null>(null);

  /**
   * Позиция маркера, в секундах.
   */
  const markerSeconds = computed(() => {
    const timelinePosition = markerX.value / timelineWidth.value;
    return props.duration * timelinePosition;
  });

  /**
   * Отформатированная позиция маркера, в часах/минутах/секундах/миллисекундах.
   */
  const markerFormatted = computed(() => {
    return formatPosition(markerSeconds.value);
  });

  /**
   * Нужно ли показать маркер.
   */
  const showMarker = computed(() => {
    return active.value &&
        markerX.value >= 0 &&
        markerX.value <= timelineWidth.value;
  });

  /**
   * Обработчики событий.
   */

  /**
   * Обработчик события установки позиции проигрывания пользователем.
   * @param value
   */
  const onPlaybackPositionChanged = (value: VideoPlaybackPosition | null) => {
    if (value != null) {
      // установить новую текущую позицию слайдера и видео
      position.value = value;

      // установить маркер в новую текущую позицию
      const positionProgress = position.value / props.duration;
      markerX.value = Math.round(timelineWidth.value * positionProgress);

      emit('position:changed', value);
    }
  };


  /**
   * Обработчик события изменения позиции проигрывания (при проигрывании видео).
   */
  const onTimeUpdate = (time: number) => {
    position.value = time;

    // на мобильных устройствах при проигрывании маркер следует за позицией воспроизведения
    if (Platform.is.mobile) {
      // установить маркер в новую текущую позицию
      const positionProgress = position.value / props.duration;
      const x = Math.round(timelineWidth.value * positionProgress);
      markerX.value = x;
    }
  };

  /**
   * Обработчик события начала и конца перетаскивания текущей позиции.
   * @param action `start` для начала перетаскивания, `end` для конца.
   */
  const onDrag = (action: string) => {
    if (action === 'start') {
      dragging.value = true;
    } else {
      dragging.value = false;
    }
  };

  /**
   * Обработчик события начала наведения курсора на индикатор позиции воспроизведения.
   */
  const onHoverStart = () => {
    if (active.value) return;
    active.value = true;

    // обновить сведения о позиции и ширине индикатора
    // (нужны для обновления позиции маркера при движении курсора над элементом индикатора)
    const timelineBox = timelineElement.value.getBoundingClientRect();
    timelineX.value = Math.round(timelineBox.x);
    timelineWidth.value = Math.round(timelineBox.width);
  };

  /**
   * Обработчик события окончания наведения курсора на индикатор позиции воспроизведения.
   */
  const onHoverEnd = () => {
    if (!active.value) return;

    // при начале перетаскивания текущей позиции происходит событие `mouseleave`;
    // не деактивировать индикатор во время перетаскивания
    if (!dragging.value) {
      active.value = false;
    }
  };

  /**
   * Обработчик события перемещения курсора над индикатором позиции воспроизведения.
   * @param event
   */
  const onMouseMove = (event: any) => {
    event.stopPropagation();
    onHoverStart();

    const x = event.clientX - timelineX.value;
    markerX.value = x;
  };

  const onWindowMouseMove = () => {
    onHoverEnd();
  };

  onBeforeMount(() => {
    window.addEventListener('mousemove', onWindowMouseMove);
  });

  onBeforeUnmount(() => {
    window.removeEventListener('mousemove', onWindowMouseMove);
  });

  watch(() => props.time, (time: number) => {
    onTimeUpdate(time);
  }, { immediate: true });
</script>

<style lang="scss" scoped>
  @import "@mixapixa-masters/styles/variables.scss";

  .video-playback-timeline {
    height: 24px;

    .q-slider {
      .q-slider__track-container {
        cursor: default;

        .q-slider__track {
          background-color: white;
        }
      }
    }
  }

  :deep .q-slider .q-slider__selection {
    transition: none;
  }

  :deep .q-slider .q-slider__thumb--h {
    transition: none;
  }

</style>

<template>
  <div
      ref="timelineElement"
      class="video-playback-timeline column items-center justify-center full-width relative-position"
  >
    <QSlider
        ref="slider"
        :class="{ 'video-playback-timeline_active': active }"
        :model-value="position"
        :label="false"
        :min="0"
        :max="duration"
        :step="0"
        :track-size="trackSize"
        :thumb-size="thumbSize"
        :disable="disabled"
        inner-track-color="master-background-control"
        :color="trackColor"
        @pan="onDrag"
        @mousemove="onMouseMove"
        @update:model-value="onPlaybackPositionChanged"
    />
    <VideoMarker
        v-if="!noMarker"
        class="video-playback-timeline__marker"
        :class="{ hidden: !showMarker }"
        :style="{
        transition: Platform.is.mobile ? 'left 0.28s' : 'none',
        'pointer-events': 'none',
      }"
        :color="markerColor"
        :textColor="markerTextColor"
        :horizontalPosition="markerX + 'px'"
        verticalPosition="14px"
        indicatorHeight="22px"
        indicatorWidth="70px"
        indicatorPinHeight="12px"
    >
      {{ markerFormatted }}
    </VideoMarker>
  </div>
</template>