<script setup lang="ts">
  import { QResizeObserver } from 'quasar';
  import TextAreaGrid from './TextAreaGrid.vue';
  import type {
    UniqueTextData,
    LooseTextElements,
    IGridData,
    CustomizerUpdateTextEvent
  } from './types';
  import { computed, nextTick, ref, watch } from 'vue';
  import type { Size, Position, TextAlignType } from '@mixapixa-masters/types';
  import { resizeFromControlPoint, rotate } from '@mixapixa-masters/utils';

  interface AddTextListProps {
    textDataList: UniqueTextData[],
    dragContainer: HTMLElement | null,
    currentDragElement: UniqueTextData | null,
    videoSize: Size | null,
    debounce?: number,
  }

  const props = withDefaults(defineProps<AddTextListProps>(), {
    debounce: 0,
  });

  const emit = defineEmits<{
    (e: 'click:text', id: string): void,
    (e: 'update:text', event: CustomizerUpdateTextEvent): void
  }>();

  /**
   * Список HTML-элементов текста, добавленного в данные момент в редактор
   */
  const textElements: LooseTextElements = {};

  /**
   * Данные области сетки (синий контрол вокруг текста). Все данные относительно нативного размера видео
   */
  const gridData = ref<IGridData>({} as IGridData);

  /**
   * Значение соотношенния размеров оригинального размера видео и размера области контейнера,
   * в который это видео вписанно
   */
  const scale = ref(1);
  const scaleY = computed(() => 100 / (props.videoSize?.height || 1));
  const scaleX = computed(() => 100 / (props.videoSize?.width || 1));

  function handlerResizeVideo(event: Size) {
    const {width} = event;
    scale.value = width / (props.videoSize?.width || width);
  }

  function onMouseDownTextElement(event: MouseEvent, id: string) {
    event.stopPropagation();
    emit('click:text', id);
  }

  function onUpdateTextAreaGrid(event: IGridData) {
    const sc = event.width / (gridData.value?.width || event.width);
    updateGridData(event);

    if (props.currentDragElement) {
      emit('update:text', {
        id: props.currentDragElement.id,
        data: {
          x: event.x,
          y: event.y,
          fontSize: props.currentDragElement.fontSize * sc,
          rotate: event.rotate
        }
      });
    }
  }

  /**
   * Возвращает размеры и координаты перетаскиваемого текста
   * @param id
   */
  function getGridDataById(id: string): IGridData | null {
    const element = textElements[id];
    const currentDragElementData = getTextDataById(id);
    if (!element || !currentDragElementData) return null;

    const {x, y, rotate} = currentDragElementData;
    const cssText = element.style.transform;
    element.style.transform = '';
    /**
     * Не берется bbox, потому что bbox не учитывает размер внутреннего контейнера, в котором текст,
     * а он больше по высоте. Потому используется scrollHeight/scrollWidth
     */
    const height = element.offsetHeight / scale.value;
    const width = element.offsetWidth / scale.value;
    element.style.transform = cssText;

    return {
      element,
      x,
      y,
      rotate,
      width,
      height,
    }
  }

  function updateGridData(data: IGridData) {
    const oldData = gridData.value;
    gridData.value = {
      ...oldData,
      ...data,
    };
  }

  /**
   * Возвращает данные об объекте текста в редакторе
   * @param id
   */
  function getTextDataById(id: string): UniqueTextData | undefined {
    return props.textDataList.find((elm) => elm.id === id);
  }

  function updateAreaTextGridById(id: string) {
    const data = getGridDataById(id);
    if (data) {
      updateGridData(data);
    }
  }

  function repositionTextOnWidthChange(oldGridData: IGridData, newGridData: IGridData, align: TextAlignType): Position {
    const pos0 = [oldGridData.x, oldGridData.y];
    const size0 = [oldGridData.width, oldGridData.height];
    const a = oldGridData.rotate * Math.PI / 180;

    const size1 = [newGridData.width, newGridData.height];
    const deltaSizeLocal = [size1[0] - size0[0], size1[1] - size0[1]];
    const deltaSizeGlobal = rotate(deltaSizeLocal[0], deltaSizeLocal[1], 0, 0, a);

    const [px, py] = {
      left: [0, 0],
      center: [0.5, 0],
      right: [1, 0]
    }[align];

    // resizeFromControlPoint определяет насколько изменились размеры, в зависимости от контрольной точки,
    //   но здесь размеры уже изменены и нужно получить delta
    deltaSizeGlobal[0] *= Math.max(px, 1 - px);
    deltaSizeGlobal[1] *= Math.max(py, 1 - py);

    const result = resizeFromControlPoint(px, py, pos0[0], pos0[1], size0[0], size0[1], a, deltaSizeGlobal[0], deltaSizeGlobal[1], 1, false);

    if (result) {
      return {
        x: result.x,
        y: result.y
      };
    } else {
      return {
        x: oldGridData.x,
        y: oldGridData.y,
      };
    }
  }

  /**
   * Располагает новый созданный текст в центре области редактора видео
   * @param id
   */
  function placeTextToCenter(id: string) {
    const data = getGridDataById(id);
    if (data && props.videoSize) {
      const {width, height} = props.videoSize;
      emit('update:text', {
        id,
        data: {
          x: width / 2 - data.width / 2,
          y: height / 2 - data.height / 2,
        }
      });
    }
  }

  /**
   * Выравнивание текста по textAlign при изменении контента
   * Вызвать функцию до применения изменений. После применения изменений вызвать то, что вернул вызов
   */
  function repositionText(id: string, textAlign: TextAlignType) {
    const oldGridData = getGridDataById(id);

    return () => {
      nextTick(() => {
        const newGridData = getGridDataById(id);
        if (oldGridData && newGridData) {
          emit('update:text', {
            id,
            data: repositionTextOnWidthChange(oldGridData, newGridData, textAlign),
          });
        }
      });
    };
  }

  function setTextElementRef(elm: any, textData: UniqueTextData) {
    if (elm) {
      textElements[textData.id] = elm as HTMLElement;
    }
  }


  watch(() => props.currentDragElement?.id || '', (newId: string, oldId: string) => {
    if (newId && newId !== oldId) {
      updateAreaTextGridById(newId);
    }
  });

  defineExpose({
    placeTextToCenter,
    repositionText,
  });
</script>

<style scoped lang="scss">
  .text-element {
    user-select: none;
    white-space: pre;
    line-height: 1;
    cursor: pointer;
  }
</style>

<template>
  <QResizeObserver
      :debounce="debounce"
      @resize="handlerResizeVideo"
  />
  <template
      v-for="textData in textDataList"
      :key="textData.id"
  >
    <div
        :ref="(elm) => setTextElementRef(elm, textData)"
        class="absolute text-element"
        :style="{
          left: `${scaleX * textData.x}%`,
          top: `${scaleY * textData.y}%`,
          padding: `${textData.fontSize * scale * 0.2}px`,
          color: textData.textColor,
          backgroundColor: textData.backgroundColor,
          fontSize: `${textData.fontSize * scale}px`,
          fontFamily: textData.fontFamily,
          textAlign: textData.textAlign,
          transform: `rotate(${textData.rotate}deg)`
        }"
        @mousedown="onMouseDownTextElement($event, textData.id)"
    >
      <span>{{ textData.text }}</span>
    </div>
  </template>
  <TextAreaGrid
      v-show="currentDragElement"
      class="target-grid"
      :grid-data="gridData"
      :scale="scale"
      :targetContainer="dragContainer"
      :style="{
        transform: `rotate(${gridData.rotate}deg)`,
        left: `${scaleX * gridData.x}%`,
        top: `${scaleY * gridData.y}%`,
        width: `${scaleX * gridData.width}%`,
        height: `${scaleY * gridData.height}%`
      }"
      @update="onUpdateTextAreaGrid"
  />
</template>