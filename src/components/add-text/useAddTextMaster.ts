import {computed, type ComputedRef, nextTick, ref} from "vue";
import type {IVideoComponent, Size} from "@mixapixa-masters/types";
import type AddTextList from "./AddTextList.vue";
import type {UniqueTextData, CustomizerUpdateTextEvent} from "./types";
import {useSetFontFamily} from "./useSetFontFamily";
import {uid} from "quasar";

export function useAddTextMaster(googleFontApiKey: string, fontFamilyList: string[], textInitData: any, videoSize: ComputedRef<Size>) {
    /**
     * Ссылка на компонент MasterVideoPlayer
     */
    const refVideo = ref<IVideoComponent | null>(null);
    /**
     * Ссылка на компонент AddTextList
     */
    const refAddTextList = ref<typeof AddTextList | null>(null);
    /**
     * Состояние видеопроигрывателя
     */
    const playState = ref(false);
    /**
     * Перетаскиваемый объект текста в текущий момент
     */
    const currentDragElement = ref<UniqueTextData | null>(null);
    const textDataList = ref<UniqueTextData[]>([]);
    const isGrid = ref(false);
    const aspectX = computed(() => videoSize.value?.width || 1);
    const aspectY = computed(() => videoSize.value?.height || 1);

    const dragContainer = computed<HTMLElement | null>(() => {
        if (refVideo.value) {
            return refVideo.value.video as HTMLElement;
        }
        return null;
    });

    const {
        createGoogleFontFaceStyle,
        initFontList,
    } = useSetFontFamily();

    /**
     * Задает состояние видео. Проигрывается оно в данный момент или нет
     * @param value
     */
    function setPlayState(value: boolean) {
        playState.value = value;
    }

    /**
     * Запускает воспроизведение видео и меняет статус видео
     */
    function play() {
        refVideo.value?.play();
        setPlayState(true);
    }

    /**
     * Останавливает воспроизведение видео и меняет статус видео
     */
    function pause() {
        refVideo.value?.pause();
        setPlayState(false);
    }

    function handlerEndedVideo() {
        setPlayState(false);
    }

    function onAddTextMounted() {
        return initFontList(fontFamilyList);
    }

    function onInstallFont(event: string) {
        createGoogleFontFaceStyle(event);
    }

    /**
     * Закрытие/удаление текстового редактора (объекта текста из редактора)
     * @param id
     */
    function handlerCloseTextEditor(id: string) {
        const index = textDataList.value.findIndex((elm) => elm.id === id);
        if (index < 0) {
            return;
        }
        textDataList.value.splice(index, 1);
    }

    /**
     * Создание нового уникального данных объекта текста
     */
    function createTextData() {
        const id = uid();

        textDataList.value.push({
            id,
            rx: 0,
            ry: 0,
            x: 0,
            y: 0,
            rotate: 0,
            ...textInitData,
        });

        /**
         * Расположение текста в центре области редактора видео
         */
        nextTick(() => {
            if (refAddTextList.value) {
                refAddTextList.value.placeTextToCenter(id);
            }
        });
    }

    function onClickCreateTextEditor() {
        createTextData();
    }

    function handlerLoadedVideo() {
        createTextData();
    }

    /**
     * Обработчик на обновление данных текста посредством текстового редактора
     * @param event
     * @param id
     */
    function onUpdateTextEditor({id, data}: CustomizerUpdateTextEvent) {
        updateTextEditorData(id, data);
        if (currentDragElement.value && currentDragElement.value.id === id) {
            currentDragElement.value = getTextDataById(id);
        }
    }

    /**
     * Обновление блока данных текстового объекта
     * @param id
     * @param data
     */
    function updateTextEditorData(id: string, data: { [key: string]: any }) {
        const oldData = getTextDataById(id);
        if (!oldData) {
            return;
        }

        let afterUpdate = null;
        if ('text' in data && data.text !== oldData.text && refAddTextList.value) {
            afterUpdate = refAddTextList.value.repositionText(id, oldData.textAlign);
        }

        const index = textDataList.value.findIndex((elm) => elm.id === id);
        const newData = {...oldData, ...data};
        const newTextDataList = textDataList.value.slice();
        newTextDataList[index] = newData;
        textDataList.value = newTextDataList;

        if (afterUpdate) {
            afterUpdate();
        }
    }

    /**
     * Возвращает данные об объекте текста в редакторе
     * @param id
     */
    function getTextDataById(id: string): UniqueTextData | null {
        return textDataList.value.find((elm) => elm.id === id) || null;
    }

    /**
     * Обработчик: клик по элементу текста в редакторе
     * @param id
     */
    function onMouseDownTextElement(id: string) {
        isGrid.value = true;
        currentDragElement.value = getTextDataById(id);
    }

    function onMouseDownBody() {
        isGrid.value = false;
        currentDragElement.value = null;
    }

    function onPositionChangedCurrentTime(time: number | null) {
        if (refVideo.value && time !== null) {
            refVideo.value.setCurrentTime(time);
        }
    }

    return {
        refVideo,
        refAddTextList,
        currentDragElement,
        dragContainer,
        aspectX,
        aspectY,
        play,
        pause,
        handlerEndedVideo,
        onAddTextMounted,
        textDataList,
        handlerCloseTextEditor,
        onInstallFont,
        onClickCreateTextEditor,
        handlerLoadedVideo,
        onUpdateTextEditor,
        onMouseDownTextElement,
        onMouseDownBody,
        onPositionChangedCurrentTime,
    }
}