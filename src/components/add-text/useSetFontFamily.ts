import * as WebFontLoader from "webfontloader";
import {ref} from "vue";

const installedFont = new Set();

export function useSetFontFamily() {
    /**
     * @deprecated createGoogleFontFaceStyle now return Promise<boolean>
     */
    const newFontLoaded = ref();

    /**
     * Создает стиль FontFace для шрифта
     * @param fontFamily
     * @param letters
     */
    function createGoogleFontFaceStyle(fontFamily: string, letters?: string): Promise<boolean> {
        return new Promise((resolve) => {
            newFontLoaded.value = false;
            const shortSuffix = letters ? '_short' : '';

            if (checkFontMap(fontFamily + shortSuffix)) {
                return resolve(false);
            }

            WebFontLoader.load({
                classes: false,
                google: {
                    families: [fontFamily],
                    text: letters,
                },
                active() {
                    fillFontMap(fontFamily + shortSuffix);
                    resolve(true);
                },
                inactive () {
                    resolve(false);
                }
            })
        });
    }

    function fillFontMap(value: string): void {
        installedFont.add(value);
    }

    function checkFontMap(value: string): boolean {
        return installedFont.has(value);
    }

    /**
     * Инициализация списка шрифтов для вывода его в интерфейс
     */
    async function initFontList(fontFamilyList: string[]) {
        if (fontFamilyList.length) {
            for (let i = 0; i < fontFamilyList.length; i += 1) {
                const fontFamily = fontFamilyList[i];
                await createGoogleFontFaceStyle(fontFamily, fontFamily);
            }
        }
    }

    return {
        createGoogleFontFaceStyle,
        initFontList,
        newFontLoaded,
    }
}