import type {TextAlignType, Position, Size} from "@mixapixa-masters/types";

export declare interface ITextEditorData {
    text: string
    textColor: string
    backgroundColor: string
    textAlign: TextAlignType
    fontFamily: string
    fontSize: number
}

export type TextEditorEvent = Partial<ITextData>;

export declare interface ITextPosition {
    x: number
    y: number
    rx: number
    ry: number
}

export declare interface ITextRotate {
    rotate: number
}

export declare interface ITextEditorFontSizeOptions {
    start: number
    end: number
    step: number
}

export declare interface ITextData extends ITextEditorData, ITextPosition, ITextRotate {
}

export declare interface UniqueTextData extends ITextData {
    id: string
}

export declare interface IGridData extends ITextRotate, Position, Size {
    element: HTMLElement
}

export declare type LooseTextElements = { [index in string]: HTMLElement };

export type UpdateObjectEvent<T> = {
    id: string,
    data: T
}

export type CustomizerUpdateTextEvent = UpdateObjectEvent<TextEditorEvent>;
export type GridUpdateTextEvent = UpdateObjectEvent<{ x: number, y: number, fontSize: number, rotate: number }>