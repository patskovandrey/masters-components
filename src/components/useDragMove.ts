import type {Position} from '@mixapixa-masters/types'

export function useDragMove() {
    let _startElementX: number;
    let _startElementY: number;
    let _startX: number;
    let _startY: number;
    let _x: number;
    let _y: number;

    function dragStart(event: any, startElementX: number, startElementY: number) {
        _startX = event.x;
        _startY = event.y;
        _startElementX = startElementX;
        _startElementY = startElementY;
        _x = startElementX;
        _y = startElementY;
    }

    /**
     * @param event
     * @param scale Значение соотношенния размеров контейнера к оригинальному размеру видео
     * @return Возвращает координаты относительно оригинального размера видео
     */
    function dragMove(event: any, scale: number): Position {
        const dx = (event.x - _startX) / scale;
        const dy = (event.y - _startY) / scale;
        _x = dx + _startElementX;
        _y = dy + _startElementY;

        return {x: _x, y: _y};
    }

    function dragEnd(): Position {
        return {x: _x, y: _y};
    }

    return {
        dragEnd,
        dragMove,
        dragStart,
    }
}