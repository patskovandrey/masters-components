export declare interface IEventReadyVideoTrim {
    readonly fileId: string,
    readonly progress: number,
    readonly message: string,
}

export declare interface IOptionsRange {
    readonly min: number
    readonly max: number
    readonly step?: number
    readonly suffix?: string
}

export declare interface IEventUpdateVideoCustomizer {
    start: number
    end: number
}