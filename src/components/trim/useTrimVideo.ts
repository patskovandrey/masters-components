import {computed, nextTick, type Ref, ref, watch} from "vue";
import type MasterVideoPlayer from '../MasterVideoPlayer.vue';
import type TrimVideoTimeline from './TrimVideoTimeline.vue';
import type VideoTrimRange from './VideoTrimRange.vue';
import type {IEventLoadedMasterVideoPlayer} from "../index";
import type {WatchStopHandle} from "@vue/runtime-core";

export function useTrimVideo(videoDuration: Ref<number>) {
    /**
     * Компонент видео плеера
     */
    const refVideo = ref<typeof MasterVideoPlayer | null>(null);
    const refTrimVideoTimeline = ref<typeof TrimVideoTimeline | null>(null);
    /**
     * Устанавливаем значение диапазона в значение 70% от своего начального размера
     */
    const rangeStart = ref(15);
    const rangeEnd = ref(85);
    const maxRange = ref(100);

    const currentTime = ref(0);
    const trimDuration = computed(() => {
        return (videoDuration.value * (rangeEnd.value - rangeStart.value)) / 100;
    });

    const playState = ref(false);
    const isHours = ref(false);
    const aspectX = ref<number>();
    const aspectY = ref<number>();
    const refVideoTrimRange = computed<typeof VideoTrimRange | null>(() => {
        return refTrimVideoTimeline.value?.refVideoTrimRange || null;
    });
    /**
     * Следит за изменением значения текущего времени
     */
    let stopWatcherCurrentTime: WatchStopHandle;

    /**
     * Хранит значение до которого будет проигрываться видео при начале проигрывания (сек)
     */
    let videoEndTime: number;

    /**
     * Конвертируент относительное значение в секунды относительно videoDuration.value
     * @param value
     */
    function relativeToSecond(value: number) {
        return (videoDuration.value * value) / 100;
    }

    /**
     * Задает текущее время видео
     * @param time
     */
    function setCurrentTime(time: number) {
        if (!refVideo.value) return;

        refVideo.value.setCurrentTime(Math.min(Math.max(time, 0), videoDuration.value));
    }

    function init(value: number) {
        videoDuration.value = value;
        isHours.value = !!Math.floor(value / (60 * 60));
        refVideoTrimRange.value?.setStart(rangeStart.value);
        refVideoTrimRange.value?.setEnd(rangeEnd.value);
        setCurrentTime(relativeToSecond(rangeStart.value));
    }

    function handlerLoadedVideo(event: IEventLoadedMasterVideoPlayer) {
        // nextTick(() => {
        init(event.duration);
        // });
        aspectX.value = event.videoWidth;
        aspectY.value = event.videoHeight;
    }

    function handlerTimeUpdateVideo(value: number) {
        currentTime.value = value;
    }

    function handlerUpdateVideoTimeline(value: number) {
        setCurrentTime(value);
    }

    function handlerUpdateVideoTrimRange(event: CustomEvent) {
        const {detail} = event;
        const {start, end, max} = detail;

        rangeStart.value = start;
        rangeEnd.value = end;
        maxRange.value = max;
    }

    function play() {
        refVideo.value?.play();
        playState.value = true;
    }

    function pause() {
        refVideo.value?.pause();
        playState.value = false;
    }

    function setMute(value: boolean) {
        refVideo.value?.setMute(value);
    }

    function setPlay(value: boolean) {
        if (refVideo.value && !videoDuration.value) {
            init(refVideo.value.duration);
        }

        if (value) {
            /**
             * Задает значение текущего времени равное начало указанного пользователем диапазона
             */
            videoEndTime = relativeToSecond(rangeEnd.value);
            setWatcherCurrentTime();
            const time = relativeToSecond(rangeStart.value);
            currentTime.value = time;
            setCurrentTime(time);
            play();
        } else {
            pause();
            stopWatcherCurrentTime();
        }
    }

    function setWatcherCurrentTime() {
        stopWatcherCurrentTime = watch(currentTime, (value: number) => {
            if (value >= videoEndTime) {
                refVideo.value?.pause();
                stopWatcherCurrentTime();
                currentTime.value = videoEndTime;
                setCurrentTime(videoEndTime);
                pause();
            }
        });
    }

    function handlerInputDragStart() {
        if (!refVideoTrimRange.value) return;
        refVideoTrimRange.value.showGhost();
    }

    function handlerInputDragEnd() {
        if (!refVideoTrimRange.value) return;
        refVideoTrimRange.value.hideGhost();
    }

    /**
     * Следит за изменением диапазона. Приоритетное изменение это начало диапазона.
     * Каждый раз при изменении диапазона меняется значение текущего времени
     */
    watch([rangeStart, rangeEnd], (value, oldValue) => {
        const [start, end] = value;
        const [oldStart] = oldValue;
        let time: number;

        if (start !== oldStart) {
            time = relativeToSecond(start);
        } else {
            time = relativeToSecond(end);
        }

        currentTime.value = time;
        setCurrentTime(time)
        if (refVideoTrimRange.value) {
            refVideoTrimRange.value.setStart(start);
            refVideoTrimRange.value.setEnd(end);
        }
    });

    return {
        refVideo,
        refTrimVideoTimeline,
        rangeStart,
        rangeEnd,
        maxRange,
        relativeToSecond,
        trimDuration,
        setPlay,
        setMute,
        currentTime,
        aspectX,
        aspectY,
        handlerLoadedVideo,
        handlerTimeUpdateVideo,
        handlerUpdateVideoTimeline,
        handlerUpdateVideoTrimRange,
        handlerInputDragStart,
        handlerInputDragEnd,
    };
}