<script lang="ts" setup>
  import SliderBody from './SliderBody.vue';
  import type { StyleValue } from 'vue';
  import { computed, ref, watch } from 'vue';
  import NodeRangeControl from './NodeRangeControl.vue';
  import { convertSeconds } from '../../utils/time';
  import { createEvent } from '../../utils/event';
  import TimelineIndicatorGhost from './TimelineIndicatorGhost.vue';
  import VideoTrimRangeGhost from './VideoTrimRangeGhost.vue';
  import { colors, QResizeObserver } from 'quasar';

  const getPaletteColor = colors.getPaletteColor;

  const props = withDefaults(defineProps<{
    duration: number
    height: string
    bgColor?: string
    bodyStyle?: StyleValue | undefined
    start?: number
    end?: number
    preloading?: boolean
    rangeControlColor?: string | undefined
    skin?: 'simple' | 'trim'
    fps?: number
  }>(), {
    bgColor: 'timeline',
    start: 0,
    end: 100,
    skin: 'simple',
    fps: 30,
  });

  const emit = defineEmits(['update']);

  enum STATE_WIDTH {
    AVERAGE = 100,
    NARROW = 50,
  }

  const refSliderContainer = ref<HTMLElement | null>(null);
  let rectSliderContainer: DOMRect;
  let widthSliderContainer: number;
  /**
   * Относительное значение начала диапазона (%)
   */
  const rangeStart = ref(props.start);
  /**
   * Относительное значение конца диапазона (%)
   */
  const rangeEnd = ref(props.end);
  /**
   * Относительное значение начала диапазона
   */
  const startControlX = ref(0);
  /**
   * Относительное значение конца диапазона
   */
  const endControlX = ref(0);
  let rangeStartX = 0;
  /**
   * Максимальная длина диапазона (%)
   */
  const maxRange = ref(100);
  const isDrag = ref(false);
  const averageSize = ref(0);
  const narrowSize = ref(0);

  /**
   * Возвращает стили размера текущего диапазона
   */
  const sliderStyle = computed(() => {
    return {
      left: `${rangeStart.value}%`,
      right: `${100 - rangeEnd.value}%`,
    };
  });

  const rangeControlColorValue = computed(() => getPaletteColor(props.rangeControlColor ?? ''));
  /**
   * Возвращает текущий размер диапазона (%)
   */
  const range = computed(() => {
    return rangeEnd.value - rangeStart.value;
  });

  /**
   * Сохраняет начальные значения начала и конца диапазона
   */
  function rememberInitialValues() {
    startControlX.value = rangeStart.value;
    endControlX.value = rangeEnd.value;
  }

  function resetInitialValues() {
    startControlX.value = -1;
    endControlX.value = -1;
  }

  /**
   * Инициализация драга. Получение актуальных размеров контейнеров, которые участвуют в вычислении значений драга
   */
  function beforeDrag() {
    if (!refSliderContainer.value) return;

    const container = refSliderContainer.value;
    widthSliderContainer = container.offsetWidth;
    rectSliderContainer = container.getBoundingClientRect();
    rememberInitialValues();
    isDrag.value = true;
  }

  function afterDrag() {
    isDrag.value = false;
    resetInitialValues();
  }

  function sendUpdate() {
    emit('update', createEvent('update', {
      start: rangeStart.value,
      end: rangeEnd.value,
      max: maxRange.value,
    }));
  }

  /**
   * Обработчик события @dragstart компонента видимого диапазона
   * @param x
   */
  function handlerDragStartRange({ x }: { x: number }) {
    beforeDrag();
    /**
     * Сохранение начального x диапазона для получения дельты изменения, чтобы
     * исключить постоянное вычисление размера диапазона
     */
    rangeStartX = x;
  }

  /**
   * Обработчик события @drag компонента видимого диапазона
   * Двигает элемент диапазона в интерфейсе
   * @param x
   */
  function handlerDragRange({ x }: { x: number }) {
    const deltaX = x - rangeStartX;
    let relativeDeltaX = deltaX / widthSliderContainer * 100;

    relativeDeltaX = Math.min(Math.max(relativeDeltaX, -startControlX.value), (100 - endControlX.value));
    setStart(startControlX.value + relativeDeltaX);
    setEnd(endControlX.value + relativeDeltaX);

    sendUpdate();
  }

  function handlerDragEnd() {
    afterDrag();
  }

  /**
   * Обработчик события @dragstart контрола, которые задает начало диапазона
   */
  function handlerDragStarControlStart() {
    beforeDrag();
  }

  /**
   * Обработчик события @drag контрола, которые задает начало диапазона
   */
  function handlerDragControlStart({ x }: { x: number }) {
    const currentX = x - rectSliderContainer.x;
    const relativeX = currentX / widthSliderContainer * 100;
    const min = Math.max(0, rangeEnd.value - maxRange.value);

    setStart(Math.min(Math.max(relativeX, min), rangeEnd.value));

    sendUpdate();
  }

  /**
   * Обработчик события @dragstart контрола, которые задает конец диапазона
   */
  function handlerDragStarControlEnd() {
    beforeDrag();
  }

  /**
   * Обработчик события @drag контрола, которые задает конец диапазона
   */
  function handlerDragControlEnd({ x }: { x: number }) {
    const currentX = x - rectSliderContainer.x;
    const relativeX = currentX / widthSliderContainer * 100;
    const max = Math.min(100, rangeStart.value + maxRange.value);

    setEnd(Math.min(Math.max(relativeX, rangeStart.value), max));
    sendUpdate();
  }

  /**
   * Задает относительное значения для положения начала диапазона (%)
   * @param value
   */
  function setStart(value: number) {
    rangeStart.value = toFramePercent(value);
  }

  /**
   * Задает относительное значения для положения конца диапазона (%)
   * @param value
   */
  function setEnd(value: number) {
    rangeEnd.value = toFramePercent(value);
  }

  /**
   * Задает относительное значения для положения длительности диапазона (%)
   * @param value
   */
  function setDuration(value: number) {
    setEnd(rangeStart.value + value);
  }

  /**
   * Задает флаг для отображения призрака шкалы диапазона
   */
  function showGhost() {
    rememberInitialValues();
    isDrag.value = true;
  }

  /**
   * Сбрасывает флаг для отображения призрака шкалы диапазона
   */
  function hideGhost() {
    isDrag.value = false;
    resetInitialValues();
  }

  function handlerResize() {
    if (refSliderContainer.value) {
      const containerWidth = refSliderContainer.value.offsetWidth;
      averageSize.value = (STATE_WIDTH.AVERAGE * 100) / containerWidth;
      narrowSize.value = (STATE_WIDTH.NARROW * 100) / containerWidth;
    }
  }

  function toFramePercent(percents: number) {
    const duration = props.duration;
    if (duration === 0) return percents;
    let t = duration * percents / 100;
    const frameTime = 1 / props.fps;
    const rem = t % frameTime;
    if (rem > frameTime / 2) {
      t += frameTime - rem;
    } else {
      t -= rem;
    }
    return Math.max(t / duration * 100, 0);
  }

  watch(() => [props.start, props.end], () => {
    setStart(props.start);
    setEnd(props.end);
  });

  defineExpose({
    setStart,
    setEnd,
    setDuration,
    showGhost,
    hideGhost,
    rangeStart,
    rangeEnd,
    maxRange,
  });
</script>

<style lang="scss" scoped>
  @import "../../styles/variables";

  .range-marker {
    width: 60px;
    text-align: center;
    height: 16px;
    bottom: -25px;
    font-size: 12px;
    transition: all ease 0.3s;

    &--start {
      left: -30px;

      &.range-marker--narrow {
        left: -60px;
      }
    }


    &--end {
      right: -30px;

      &.range-marker--narrow {
        right: -60px;
      }
    }

    &--center {
      left: 50%;
      transform: translateX(-30px);

      &.center-hidden {
        opacity: 0;
      }
    }
  }

  .trim-range-skin {
    &--trim {
      @mixin trimBody {
        content: '';
        position: absolute;
        width: 100%;
        height: 2px;
        background: v-bind(rangeControlColorValue);
      }

      .trim-range__body {
        &:after {
          @include trimBody;
          top: -5px;
        }

        &:before {
          @include trimBody;
          bottom: -5px;
        }
      }
    }
  }
</style>

<template>
  <div
    class="relative-position col-grow items-center flex"
    :class="`trim-range-skin--${skin}`"
    :style="{
        height: height
      }"
    ref="refSliderContainer"
  >
    <QResizeObserver
      @resize="handlerResize"
    />
    <VideoTrimRangeGhost
      :class="'bg-'+props.bgColor"
      :style="{
          left: startControlX + '%',
          right: 100 - endControlX + '%',
        }"
    />
    <SliderBody
      :style="[sliderStyle, props.bodyStyle]"
      :height="height"
      class="cursor-pointer trim-range__body"
      :color="props.bgColor"
      text-color="master-text-color"
      v-draggable="{overlayCursor: 'auto'}"
      @dragstart="handlerDragStartRange"
      @drag="handlerDragRange"
      @dragend="handlerDragEnd"
    >
      <NodeRangeControl
        side="left"
        style="z-index: 1"
        overlayCursor="e-resize"
        :skin="skin"
        :color="rangeControlColor"
        :height="height"
        @dragstart="handlerDragStarControlStart"
        @drag="handlerDragControlStart"
        @dragend="handlerDragEnd"
      />
      <NodeRangeControl
        side="right"
        style="z-index: 1"
        overlayCursor="e-resize"
        :skin="skin"
        :height="height"
        :color="rangeControlColor"
        @dragstart="handlerDragStarControlEnd"
        @drag="handlerDragControlEnd"
        @dragend="handlerDragEnd"
      />
      <slot/>
      <template v-if="!preloading">
        <div
          :class="['absolute range-marker range-marker--start no-pointer-events non-selectable', range <= narrowSize && 'range-marker--narrow']"
        >
          {{ convertSeconds(duration * rangeStart / 100) }}
        </div>
        <div
          :class="['absolute range-marker range-marker--end no-pointer-events non-selectable', range <= narrowSize && 'range-marker--narrow']"
        >
          {{ convertSeconds(duration * rangeEnd / 100) }}
        </div>
        <div
          class="absolute range-marker range-marker--center no-pointer-events non-selectable"
          :class="range < averageSize && 'center-hidden'"
        >
          {{ convertSeconds(Math.floor(duration * range) / 100) }}
        </div>
      </template>
    </SliderBody>

    <TimelineIndicatorGhost
      :height="height"
      v-show="isDrag"
      class="text-black"
      :style="{left: startControlX + '%'}"
    />
    <TimelineIndicatorGhost
      :height="height"
      v-show="isDrag"
      class="text-black"
      :style="{left: endControlX + '%'}"
    />

  </div>
</template>