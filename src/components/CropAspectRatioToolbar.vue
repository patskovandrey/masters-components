<script lang="ts" setup>
  /**
   * Панель инструментов: изменение соотношения сторон области кадрирования.
   *
   * Содержит поля ввода пользовательского соотношения сторон
   * и кнопки выбора предопределенных соотношений сторон.
   *
   * Свойства: см. `CropAspectRatioToolbarProps`.
   * События: см. `CropAspectRatioToolbarEmits`.
   */

  import type { CropAspectRatio } from '@mixapixa-masters/types';
  import { MasterDraggableInput, PropertyLabel, ToggleIconTextButton } from '../index';
  import { computed } from 'vue';
  import { setupI18n } from '@mixapixa-masters/locales';

  interface CropAspectRatioToolbarProps {
    // подсветка текущего значения
    active?: boolean;
    // текущее соотношение сторон области кадрирования
    cropAspectRatio: CropAspectRatio;
    cropAspectRatios: CropAspectRatio[];
    preloading?: boolean;
    bgColor?: string;
    label?: string;
  }

  interface CropAspectRatioToolbarEmits {
    // изменилось текущее соотношение сторон области кадрирования
    (event: 'aspect-ratio:changed', payload: CropAspectRatio): void,
  }

  const props = withDefaults(defineProps<CropAspectRatioToolbarProps>(), {
    active: true,
    bgColor: 'master-background-light',
    cropAspectRatios: () => [
      {
        width: 1,
        height: 1,
        label: '1:1',
      },
      {
        width: 16,
        height: 9,
        label: '16:9',
      },
      {
        width: 9,
        height: 16,
        label: '9:16',
      },
      {
        width: 4,
        height: 3,
        label: '4:3',
      },
      {
        width: 3,
        height: 4,
        label: '3:4',
      },
      {
        width: 2,
        height: 1,
        label: '2:1',
      },
      {
        width: 1,
        height: 2,
        label: '1:2',
      },
    ],
  });
  const { t } = setupI18n();
  const emit = defineEmits<CropAspectRatioToolbarEmits>();
  const normalizedAspect = computed<CropAspectRatio>(() => {
    const aspect = props.cropAspectRatio;
    if (!aspect.width || !aspect.height) {
      return aspect;
    }
    const gcd = greatestCommonDivisor(aspect.width, aspect.height);
    if (gcd === 1) {
      return { ...aspect };
    } else {
      return {
        ...aspect,
        width: aspect.width / gcd,
        height: aspect.height / gcd,
      };
    }
  });

  const options = computed(() => {
    return {
      min: 1,
    };
  });

  /**
   * Являются ли два соотношения сторон эквивалентными.
   * @param aspectRatio1
   * @param aspectRatio2
   */
  const isEqual = (
      aspectRatio1: CropAspectRatio,
      aspectRatio2: CropAspectRatio,
  ): boolean => {
    return (
        aspectRatio1.width === aspectRatio2.width &&
        aspectRatio1.height === aspectRatio2.height
    );
  };

  /**
   * Получение наибольшего общего делителя
   */
  const greatestCommonDivisor = (a: number, b: number): number => {
    if (!b) {
      return Math.abs(a);
    }
    return greatestCommonDivisor(b, a % b);
  };

  function parse(value: string, oldValue: number | undefined) {
    // if (value === '') return null;
    const result = +value;
    if (oldValue !== undefined && Number.isNaN(result)) {
      return oldValue;
    }

    return result;
  }

  function format(value: number | null) {
    if (value === null) return '';
    return Math.round(value).toString();
  }


  /**
   * Обработчик клика по кнопке выбора соотношения сторон области кадрирования.
   * @param aspectRatio
   */
  const onAspectRatioButtonClick = (aspectRatio: CropAspectRatio) => {
    emit('aspect-ratio:changed', { ...aspectRatio });
  };

  function handlerInputDragAspectWidth(value: number | string) {
    const result = Math.round(+value);

    emit('aspect-ratio:changed', {
      width: result,
      height: normalizedAspect.value.height,
    });
  }

  function handlerInputDragAspectHeight(value: number | string) {
    const result = Math.round(+value);

    emit('aspect-ratio:changed', {
      width: normalizedAspect.value.width,
      height: result,
    });
  }
</script>

<style lang="scss" scoped>
  $height: 32px;

  .container {
    display: grid;
    grid-template-columns: 1fr 60px 24px 60px;
    grid-template-rows: 1fr;
    grid-auto-columns: 1fr;
    gap: 0 8px;
    grid-auto-flow: row dense;
    align-items: center;
    grid-template-areas:
    ". . . .";
  }

  .buttons-grid {
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-template-rows: repeat(2, $height);
    grid-column-gap: 7px;
    grid-row-gap: 7px;
  }

  @media screen and (max-width: 1023px) {
    .container {
      grid-template-columns: minmax(40px, 100px) minmax(40px, 100px) 24px minmax(40px, 100px);
    }
  }
</style>

<template>
  <div class="container q-pb-md">
    <PropertyLabel
      :label="label || t('proportion')"
      :preloading="preloading"
    />
    <MasterDraggableInput
      class="text-master-brand"
      :preloading="preloading"
      :value="normalizedAspect.width"
      :options="options"
      :format="format"
      :parse="(e: string) => parse(e, normalizedAspect.width)"
      @input:drag="handlerInputDragAspectWidth"
      @change="handlerInputDragAspectWidth"
    />
    <PropertyLabel
      class="text-center q-px-xs"
      label=":"
      :preloading="preloading"
    />
    <MasterDraggableInput
      :preloading="preloading"
      class="text-master-brand"
      :value="normalizedAspect.height"
      :options="options"
      :format="format"
      :parse="(e: string) => parse(e, normalizedAspect.height)"
      @input:drag="handlerInputDragAspectHeight"
      @change="handlerInputDragAspectHeight"
    />
  </div>
  <div class="buttons-grid">
    <template
      v-for="aspectRatio in cropAspectRatios"
      :key="aspectRatio.label"
    >
      <ToggleIconTextButton
        class="text-weight-regular"
        :bg-color="bgColor"
        :label="aspectRatio.label"
        :model-value="active ? isEqual({width: aspectRatio.width, height: aspectRatio.height}, normalizedAspect) : false"
        :preloading="preloading"
        @click="onAspectRatioButtonClick({width: aspectRatio.width, height: aspectRatio.height})"
      />
    </template>
  </div>
</template>