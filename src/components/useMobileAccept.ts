import {computed} from "vue";
import {Platform} from "quasar";

export function useMobileAccept(props: any) {

    // Возвращает undefined на мобильных устройствах
    const uploaderAccept = computed<string | undefined>(() => {
        return Platform.is.mobile ? undefined : props.accept;
    });

    return {uploaderAccept};
}