import type {ComponentPublicInstance} from "vue";

export declare interface IMenuItemComponentProps {
    readonly [index in T]: unknown
}

export declare interface IMenuItemData {
    readonly name?: string
    readonly label?: string
    readonly click?: (e: MouseEvent) => e
    readonly icon?: string
    readonly contentClass?: string
    readonly preventClick?: boolean
    readonly component?: unknown | ComponentPublicInstance
    readonly componentProps?: IMenuItemComponentProps
    readonly buttonText?: string
}

export declare interface IOptionsRange {
    readonly min: number
    readonly max: number
    readonly step?: number
    readonly suffix?: string
}

export declare interface IOptionsRange {
    readonly min: number
    readonly max: number
    readonly step?: number
    readonly suffix?: string
}

export declare interface IEventLoadedMasterVideoPlayer {
    readonly duration: number
    readonly videoWidth: number
    readonly videoHeight: number
}

export declare interface IEventLoadedMasterGifPlayer {
    readonly naturalWidth: number
    readonly naturalHeight: number
}

export declare interface IMasterDraggableInputOptions {
    min?: number
    max?: number
    suffix?: string
    step?: number
}

export declare type TwoDimensionalNumericArrayType = [number, number]