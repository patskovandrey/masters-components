type FileType = 'image' | 'video' | 'audio' | 'font' | 'svg';
type LooseNumber = { [s: string]: number };
type LooseBoolean = { [s: string]: boolean };
type LooseFileType = { [s: string]: FileType };

/**
 * Тип файлов-изображений
 */
const FILE_IMAGE_TYPES: LooseBoolean = {
    'image/png': true,
    // 'image/svg+xml': true,
    'image/tiff': true,
    // 'image/vnd.wap.wbmp': true,
    // 'image/webp': true,
    // 'image/x-icon': true,
    // 'image/x-jng': true,
    // 'image/x-ms-bmp': true,
    'image/jpeg': true,
    'image/bmp': true,
};

/**
 * Тип файлов-видео
 */
const FILE_VIDEO_TYPES: LooseBoolean = {
    'video/3gpp': true,
    'video/mp2t': true,
    'video/mp4': true,
    'video/avi': true,
    'video/mpeg': true,
    'video/quicktime': true, // mov
    'video/webm': true, //.webm
    'video/x-flv': true,
    'video/x-m4v': true,
    'video/x-mng': true,
    'video/x-ms-asf': true,
    'video/x-ms-wmv': true,
    'video/x-msvideo': true,
    'video/x-matroska': true, // mkv
    'video/ogg': true, //.ogm, .ogv, .ogg
};

/**
 * Типы файлов-аудио
 */
const FILE_AUDIO_TYPES: LooseBoolean = {
    'audio/aac': true,
    // 'audio/vnd.dlna.adts': true, // aac
    'audio/mpeg': true, // mp3
    'audio/mp3': true, // mp3 chrome old bug
    // 'audio/wave': true,
    'audio/wav': true,
    // 'audio/x-wav': true,
    // 'audio/x-pn-wav': true,
};

/**
 * Тип файлов-svg
 */
const FILE_SVG_TYPES: LooseBoolean = {
    'image/svg+xml': true,
};

/**
 *  Тип файлов-шрифтов
 * @type {Object}
 */
const FILE_FONT_TYPES: LooseBoolean = {
    otf: true,
    ttf: true,
};

const FILE_TYPES: LooseFileType = {
    IMAGE: 'image',
    VIDEO: 'video',
    AUDIO: 'audio',
    FONT: 'font',
    SVG: 'svg',
};

/**
 *  Содержит максимальные размеры файлов по типам в мегабайтах
 * @enum {Object}
 */
const MAX_FILE_SIZE_IN_MB: LooseNumber = {
    [FILE_TYPES.IMAGE]: 10,
    [FILE_TYPES.VIDEO]: 5,
    [FILE_TYPES.FONT]: 10,
    [FILE_TYPES.AUDIO]: 10,
    [FILE_TYPES.SVG]: 10,
};

/**
 *  Содержит максимальные размеры файлов по типам
 */
const MAX_FILE_SIZE: LooseNumber = {
    [FILE_TYPES.IMAGE]: MAX_FILE_SIZE_IN_MB[FILE_TYPES.IMAGE] * 1024 * 1024,
    [FILE_TYPES.VIDEO]: MAX_FILE_SIZE_IN_MB[FILE_TYPES.VIDEO] * 1024 * 1024,
    [FILE_TYPES.FONT]: MAX_FILE_SIZE_IN_MB[FILE_TYPES.FONT] * 1024 * 1024,
    [FILE_TYPES.AUDIO]: MAX_FILE_SIZE_IN_MB[FILE_TYPES.AUDIO] * 1024 * 1024,
    [FILE_TYPES.SVG]: MAX_FILE_SIZE_IN_MB[FILE_TYPES.SVG] * 1024 * 1024,
};

/**
 *  Проверяет размер файла на допустимое значение, в зависимости от переданного типа файла
 * @param {Number} fileSize Размер файла
 * @param {FileType} fileType Тип файла
 * @return {Boolean}
 */
function checkFileSize(fileSize: number, fileType: FileType): boolean {
    return fileSize <= MAX_FILE_SIZE[fileType];
}

function getVideoFileType(fileType: FileType) {
    return FILE_VIDEO_TYPES[fileType] ? FILE_TYPES.VIDEO : '';
}

function formatBytes(bytes: number, decimals = 2): string {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

export {
    FILE_AUDIO_TYPES,
    FILE_FONT_TYPES,
    FILE_IMAGE_TYPES,
    FILE_SVG_TYPES,
    FILE_VIDEO_TYPES,
    MAX_FILE_SIZE_IN_MB,
    checkFileSize,
    formatBytes,
    getVideoFileType,
};