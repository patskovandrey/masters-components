import {Platform, Screen} from "quasar";
import {computed} from "vue";

/**
 * Флаг, определяющий что тип устройства является мобильным
 */
const isMobile = Platform.is.mobile;
const isMobileBehavior = computed(() => Screen.lt.md);

export {
    isMobile,
    isMobileBehavior,
}