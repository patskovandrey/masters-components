const convert = (x: number) => {
    return (x < 10) ? '0' + x : x;
};

/**
 * Конвертирует секунды в формат hh:mm:ss.ms
 * @see @mixapixa-masters/utils - convertSecondsToTimeFormat
 *
 * @param seconds
 * @param isHour
 * @deprecated
 */
function convertSeconds(seconds: number, isHour?: boolean): string {
    let out = '';
    if (isHour) {
        out = convert(Math.floor((seconds / (60 * 60)))) + ':';
    }

    out += convert(Math.floor((seconds / 60 % 60))) + ':';
    out += convert(Math.floor((seconds % 60)));

    const strS = seconds.toString(10);
    const indexDot = strS.indexOf(('.'));
    if (~indexDot) {
        out += '.' + strS.substring(indexDot + 1, indexDot + 2);
    } else {
        out += '.' + 0;
    }

    return out;
}

export {
    convertSeconds,
};