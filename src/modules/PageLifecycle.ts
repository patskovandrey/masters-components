import Statistics from '@mixapixa-masters/statistics'

export declare interface IPageLifecycle {
    addUnsavedChanges: (e: string) => void
    removeUnsavedChanges: (e: string) => void
}

class ClassPageLifecycle {
    protected changes: string[];

    constructor() {
        this.changes = [];
    }

    protected handlerWindowBeforeUnload(event: BeforeUnloadEvent) {
        event.returnValue = '';
        event.preventDefault();
        Statistics.sendEvent('event', {
            act: 'close_tab'
        });
    }

    private addEventListener() {
        window.addEventListener('beforeunload', this.handlerWindowBeforeUnload);
    }

    private removeEventListener() {
        window.removeEventListener('beforeunload', this.handlerWindowBeforeUnload);
    }

    public addUnsavedChanges(id: string) {
        if (!this.changes.length) {
            this.addEventListener();
        }
        if (!~this.changes.indexOf(id)) {
            this.changes.push(id);
        }
    }

    public removeUnsavedChanges(id: string) {
        if (!id) {
            return;
        }

        const index = this.changes.indexOf(id);
        if (index < 0) {
            return;
        }

        this.changes.splice(this.changes.indexOf(id), 1);
        if (!this.changes.length) {
            this.removeEventListener();
        }
    }
}

export const PageLifecycle: IPageLifecycle = new ClassPageLifecycle();

export default ClassPageLifecycle;