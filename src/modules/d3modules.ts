import {select, selection, pointer} from 'd3-selection';
import {drag} from 'd3-drag';

export default {
    select, selection, pointer,
    drag,
    sourceEvent(event: any) {
        let sourceEvent;
        while ((sourceEvent = event.sourceEvent)) event = sourceEvent;
        return event;
    }
};