import type {QuasarPluginOptions} from "quasar";
import type {Plugin} from "vue";
import {Quasar, Dialog, Loading, Notify} from "quasar";

export function getQuasarPlugin(options: Partial<QuasarPluginOptions> = {}): [Plugin, Partial<QuasarPluginOptions>] {
    return [
        Quasar,
        {
            ...options,
            plugins: {
                Notify,
                Dialog,
                Loading,
                ...(options.plugins || {}),
            },
            config: {
                loading: {
                    delay: 100,
                    ...(options.config?.loading || {})
                },
            },
        },
    ];
}

export {useQuasar} from 'quasar';