import {addAudio, addText, compress, crop, loop, merge, resize, speed, trim, volumeOff} from '../icons';
import type {ApplicationID} from '@mixapixa-masters/types';

interface toolItem {
    label: string;
    icon: string;
    // this id generates the url path to the masters
    id: ApplicationID | 'add-text-to-video' | 'add-audio-to-video';
}

export const toolsList: toolItem[] = [{
    label: 'Trim video',
    icon: trim,
    id: 'trim-video',
}, {
    label: 'Merge videos',
    icon: merge,
    id: 'merge-videos',
}, {
    label: 'Compress video',
    icon: compress,
    id: 'compress-video',
}, {
    label: 'Crop video',
    icon: crop,
    id: 'crop-video',
}, {
    label: 'Resize video',
    icon: resize,
    id: 'resize-video',
}, {
    label: 'Change video speed',
    icon: speed,
    id: 'change-video-speed',
}, {
    label: 'Loop video',
    icon: loop,
    id: 'loop-video',
}, {
    label: 'Mute video',
    icon: volumeOff,
    id: 'mute-video',
}, {
    label: 'Add audio to video',
    icon: addAudio,
    id: 'add-audio-to-video',
}, {
    label: 'Add text to video',
    icon: addText,
    id: 'add-text-to-video',
}];