import type {AxiosResponse} from 'axios';

export function requestWithAuthorize<T = AxiosResponse>(requestCallback: () => Promise<T>, authCallback: () => Promise<boolean>): Promise<T> {
    return new Promise((resolve, reject) => {
        requestCallback()
            .then(resolve)
            .catch(async (err: any) => {
                if (err.response?.status === 401 && await authCallback()) {
                    requestWithAuthorize(requestCallback, authCallback)
                        .then(resolve)
                        .catch(reject);
                } else {
                    reject(err);
                }
            });
    })
}