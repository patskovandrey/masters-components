import type {AuthStage} from '@mixapixa-masters/types';

export interface InitAuthorizationParams {
    runAuthorize: (stage: AuthStage, afterAuth?: () => void) => any,
    requestPayment: ({next}: {next?: () => any}) => any,
    closeAuth: () => void,
}

let authFunction: InitAuthorizationParams | null = null;
export function initAuthorization(params: InitAuthorizationParams) {
    authFunction = params;
}

export async function runAuthorize(stage: AuthStage, afterAuth?: () => void) {
    return authFunction?.runAuthorize(stage, afterAuth);
}

export function requestPayment(params: {next?: () => any}) {
    return authFunction?.requestPayment?.(params);
}