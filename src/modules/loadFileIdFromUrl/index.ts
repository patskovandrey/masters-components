import type {LoadFileIdResult} from "@mixapixa-masters/types";
import * as loaders from './loaders';
import type {RouteLocationNormalizedLoaded} from "vue-router";

type Loaders = Record<string, any>;

export async function loadFileIdFromUrl(url: string | RouteLocationNormalizedLoaded): Promise<LoadFileIdResult> {
    let parsedUrl;
    if (typeof url === 'string') {
        parsedUrl = new URL(url);
    } else {
        parsedUrl = url;
    }
    const from = getFileSourceName(url);
    const loader = getLoader(from);
    if (loader) {
        return loader.load(parsedUrl);
    } else {
        return {fileId: ''};
    }
}

export function hasFileId(url: string | URL | RouteLocationNormalizedLoaded): boolean {
    if (typeof url === 'string' || url instanceof URL) {
        const parsedUrl = new URL(url);
        return !!(parsedUrl.searchParams.get('from') || parsedUrl.searchParams.get('fileid'));
    } else {
        return !!(url.query.from?.[0] || url.query.from || url.query.fileid);
    }
}

export function getFileSourceName(url: string | URL | RouteLocationNormalizedLoaded): string {
    if (typeof url === 'string' || url instanceof URL) {
        const parsedUrl = new URL(url);
        return parsedUrl.searchParams.get('from') || 'masters';
    } else {
        return url.query.from as string || 'masters';
    }
}

function getLoader(name: string) {
    if (!(loaders as Loaders)[name]) {
        throw new Error('UNKNOWN_FILEID_LOADER: ' + name);
    }
    return (loaders as Loaders)[name];
}