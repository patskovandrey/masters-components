import type {LoadFileIdResult} from '@mixapixa-masters/types';
import type {RouteLocationNormalizedLoaded} from "vue-router";

export function load(url: URL | RouteLocationNormalizedLoaded): LoadFileIdResult {
    if (url instanceof URL) {
        return {fileId: url.searchParams.get('fileid') || ''};
    } else {
        const fileId = url.query.fileid;
        if (Array.isArray(fileId)) {
            return {fileId: fileId[0] || ''};
        } else {
            return {fileId: fileId || ''};
        }
    }
}