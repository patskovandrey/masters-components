import type {LoadFileIdResult} from '@mixapixa-masters/types';
import {Providers} from '@mixapixa-masters/providers';
import {requestWithAuthorize} from "../../requestWithAuthorize";
import type {AxiosResponse} from "axios";
import type {RouteLocationNormalizedLoaded} from "vue-router";

export async function load(url: URL | RouteLocationNormalizedLoaded): Promise<LoadFileIdResult> {
    const dropboxProvider = Providers.getProvider<Providers.dropbox.DropboxProvider>('dropbox');
    const provider = Providers.getProvider<Providers.masters.Provider>('masters');
    let sourceFileId: string;
    if (url instanceof URL) {
        sourceFileId = url.searchParams.get('file_id') || '';
    } else {
        sourceFileId = (Array.isArray(url.query.file_id) ? url.query.file_id[0] : url.query.file_id) || '';
    }
    if (!sourceFileId) return {fileId: ''};
    let accessToken = await getDropboxAccessToken(dropboxProvider);
    if (!accessToken) return {fileId: ''};

    const response = await requestWithAuthorize<AxiosResponse<LoadFileIdResult>>(() => {
        return provider.post('/dropbox/load-file', {
            path: sourceFileId,
            accessToken,
        }, {
            responseType: 'json',
        })
    }, async () => {
        await dropboxProvider.tryAuthorize();
        accessToken = dropboxProvider.getAccessToken();
        return dropboxProvider.isAuthorized();
    })
        .catch(() => {
            throw new Error('ERROR_LOAD_FILE');
        });


    if (response) {
        return response.data;
    } else {
        return {fileId: ''};
    }
}

async function getDropboxAccessToken(dropboxProvider: Providers.dropbox.DropboxProvider) {
    if (dropboxProvider.isAuthorized()) {
        return dropboxProvider.getAccessToken();
    }

    if (await dropboxProvider.tryAuthorize()) {
        return dropboxProvider.getAccessToken();
    }

    return '';
}