import {Notify} from "quasar";
import {alert} from '../icons';

/**
 * Перед использованием необходимо подключить плагин Notify к Quasar
 */
export function errorNotify(message: string) {
    return Notify.create({
        message,
        type: 'negative',
        html: true,
        icon: alert,
    });
}