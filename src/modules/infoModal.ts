import {Dialog} from "quasar";

export function infoModal(message: string) {
    return Dialog.create({
        title: 'Info',
        message,
        noBackdropDismiss: true,
    });
}