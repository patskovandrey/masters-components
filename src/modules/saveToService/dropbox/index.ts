import {requestWithAuthorize} from "../../requestWithAuthorize";
import {Providers} from '@mixapixa-masters/providers';

export async function runSave(fileId: string, metadata: any) {
    const path = metadata.path_lower;
    if (!path) return;

    const dropboxProvider = Providers.getProvider<Providers.dropbox.DropboxProvider>('dropbox');
    const provider = Providers.getProvider<Providers.masters.Provider>('masters');
    let accessToken = dropboxProvider.getAccessToken();
    const response = await requestWithAuthorize(() => {
        return provider.post('/dropbox/upload-file', {
            fileId,
            sourceFilePath: path,
            accessToken,
        }, {
            responseType: 'json'
        });
    }, async () => {
        await dropboxProvider.tryAuthorize();
        accessToken = dropboxProvider.getAccessToken();
        return dropboxProvider.isAuthorized();
    });
    if (response) {
        return response.data;
    }
    return null;
}