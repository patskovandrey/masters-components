import * as savers from './savers';

type Savers = Record<string, any>;

export async function saveToService(fileId: string, name: string, metadata: any) {
    const saver = getSaver(name);
    return saver.runSave(fileId, metadata);
}

function getSaver(name: string) {
    if (!(savers as Savers)[name]) {
        throw new Error('UNKNOWN_SERVICE_NAME: ' + name);
    }
    return (savers as Savers)[name];
}