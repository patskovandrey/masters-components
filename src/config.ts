import type {MPAppConfig} from './config.desc';

const Config: MPAppConfig = {
    title: 'Trim video',
    serviceUrl: 'http://test.masters.mixapixa.com',
    socketUrl: 'http://test.masters.mixapixa.com'
}

export default Config;